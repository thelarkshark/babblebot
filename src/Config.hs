{-# LANGUAGE DeriveGeneric #-}

module Config
  ( Config(..)
  , AgentConfig(..)
  , BotConfig(..)
  , ChannelConfig(..)
  , IrcState(..)
  ) where

import GHC.Generics (Generic)
import Data.Aeson (FromJSON)
import Data.Acid (AcidState)

import State

data Config = Config { admin :: Maybe String, agent :: Maybe AgentConfig, bots :: [BotConfig] } deriving (Show, Generic)
instance FromJSON Config

data AgentConfig = AgentConfig { port :: Int, secret :: String } deriving (Show, Generic)
instance FromJSON AgentConfig

data BotConfig = BotConfig { bot :: String, passphrase :: String, client_id :: String, discord_token :: Maybe String, channels :: [ChannelConfig] } deriving (Show, Generic)
instance FromJSON BotConfig

data ChannelConfig = ChannelConfig { channel :: String, oauth :: String, web_port :: Int, web_user :: String, web_pass :: String, discord_id :: Maybe String } deriving (Show, Generic)
instance FromJSON ChannelConfig

data IrcState = IrcState { db :: AcidState BabbleState, botConfig :: BotConfig, channelConfigs :: [ChannelConfig] }
