{-# LANGUAGE OverloadedStrings #-}

module Util
  ( fetchIRCState
  , safeGetWith
  , safePostWith
  , safePutWith
  , fromMsgEvent
  , modCheck
  , parseVar
  , safeRegex
  , urlRegex
  , logMsg
  ) where

import System.IO
import System.IO.Error
import Data.Either (fromRight)
import Data.Aeson (Value(..), decode)
import Data.Acid (AcidState, query, update)
import Data.Time.Clock (UTCTime, getCurrentTime)
import Data.Text.ICU.Regex (Regex, MatchOption, start_, end_, find, findNext, setText, clone, regex)
import Data.Text.ICU.Error (ParseError, ICUError)
import Control.Lens ((^.), (.~), (&))
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent.STM.TVar (readTVarIO)
import Network.HTTP.Client (HttpException)
import Network.IRC.Client
import qualified Network.Wreq as W
import qualified Data.List as L (find, intercalate)
import qualified Data.Text as T (Text, unpack, pack)
import qualified Data.Vector as V (any, toList)
import qualified Data.HashMap.Strict as HM (lookup)
import qualified Data.ByteString.Char8 as C (pack)
import qualified Data.ByteString.Lazy as LB (ByteString)
import qualified Control.Exception as E

import State
import Config

safeRegex :: [MatchOption] -> String -> IO (Either String Regex)
safeRegex opts str = parseRegex `E.catches` [E.Handler regexPHandler, E.Handler regexIHandler]
  where
    parseRegex = do
      regex' <- regex opts (T.pack str)
      pure $ Right regex'
    regexPHandler :: ParseError -> IO (Either String Regex)
    regexPHandler e = pure $ Left $ show e
    regexIHandler :: ICUError -> IO (Either String Regex)
    regexIHandler e = pure $ Left $ show e

httpEHandler :: HttpException -> IO (Either String (W.Response LB.ByteString))
httpEHandler e = pure $ Left $ show e

safeGetWith :: W.Options -> String -> IO (Either String (W.Response LB.ByteString))
safeGetWith opts url = fetchURL `E.catch` httpEHandler
  where
    fetchURL = do
      rsp <- W.getWith opts url
      pure $ Right rsp

safePostWith :: W.Options -> String -> Value -> IO (Either String (W.Response LB.ByteString))
safePostWith opts url data' = fetchURL `E.catch` httpEHandler
  where
    fetchURL = do
      rsp <- W.postWith opts url data'
      pure $ Right rsp

safePutWith :: W.Options -> String -> Value -> IO (Either String (W.Response LB.ByteString))
safePutWith opts url data' = fetchURL `E.catch` httpEHandler
  where
    fetchURL = do
      rsp <- W.putWith opts url data'
      pure $ Right rsp

fetchIRCState :: IRC IrcState IrcState
fetchIRCState = do
  istate <- get userState <$> getIRCState
  liftIO $ readTVarIO istate

fromMsgEvent :: Source T.Text -> Either a0 T.Text -> (String, String)
fromMsgEvent src msg'' =
  let Network.IRC.Client.Channel _ nick' = src
      msg' = fromRight (T.pack "") msg''
      msg  = T.unpack msg'
      nick = T.unpack nick'
  in (nick,msg)

modCheck :: AcidState BabbleState -> BotConfig -> ChannelConfig -> String -> IO Bool
modCheck st b c n = do
  let opts = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Client-ID" .~ [C.pack $ client_id b]
  rsp' <- safeGetWith opts ("http://tmi.twitch.tv/group/user/" ++ channel c ++ "/chatters")
  case rsp' of
    Left err -> pure False
    Right rsp -> do
      let body = decode $ rsp ^. W.responseBody :: Maybe Value
      case body of
        Nothing -> pure False
        Just (Object o) ->
          case HM.lookup "chatters" o of
            Nothing -> pure False
            Just (Object o) ->
              case HM.lookup "moderators" o of
                Just (Array a) ->
                  case L.find (n==) $ map (\(String s) -> T.unpack s) (V.toList a) of
                    Nothing -> pure False
                    Just s  -> update st (AddMod (channel c) s) >> pure True

parseVar :: String -> String -> ([String] -> IO String) -> IO String
parseVar str var f = do
  regex' <- regex [] $ T.pack $ "\\(" ++ var ++ "((?: [\\w\\-:/]+)*)\\)"
  clonedR <- clone regex'
  parseNext clonedR str
  where
    parseNext clonedR str = do
      setText clonedR (T.pack str)
      found <- findNext clonedR
      if found then do
        startI  <- start_ clonedR 0
        endI    <- end_ clonedR 0
        startI' <- start_ clonedR 1
        endI'   <- end_ clonedR 1
        cmdR    <- f $ words $ take (fromIntegral endI' - fromIntegral startI') (drop (fromIntegral startI') str)
        parseNext clonedR $ (take (fromIntegral startI) str) ++ cmdR ++ (drop (length str - (length str - fromIntegral endI)) str)
      else
        pure str

logMsg :: AcidState BabbleState -> ChannelConfig -> LogLevel -> String -> IO ()
logMsg state chan lvl str = do
  let msg = unwords $ words $ L.intercalate " " $ lines str
  now <- getCurrentTime
  update state $ AddLogMessage (channel chan) now lvl msg
  putStrLn $ "[" ++ lvl' ++ "] " ++ msg
  where
    lvl' = case lvl of
             Error -> "ERROR"
             Info  -> "INFO"

urlRegex = T.pack "(((ht|f)tp(s?))\\:\\/\\/)?[a-zA-Z0-9\\-\\.]+(?<!\\.)\\.(aero|arpa|biz|cat|com|coop|edu|gov|info|jobs|mil|mobi|museum|name|net|org|pro|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|ap|aq|ar|as|at|au|aw|az|ax|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)(\\:[0-9]+)*(\\/($|[a-zA-Z0-9\\.\\,\\;\\?\\'\\\\\\+&%\\$#\\=~_\\-]+))*"
