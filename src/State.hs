{-# LANGUAGE OverloadedStrings, TemplateHaskell, DeriveGeneric, RecordWildCards, TypeFamilies #-}

module State where

import GHC.Generics (Generic)
import GHC.Int (Int64)
import Data.Aeson (ToJSON, FromJSON)
import Data.Char (toLower)
import Data.Word (Word32)
import Data.Time.Clock (UTCTime)
import Data.Acid (Update, Query, makeAcidic)
import Data.SafeCopy (Migrate, MigrateFrom, migrate, deriveSafeCopy, base, extension)
import Control.Monad.Reader (ask)
import qualified Data.List as L (filter)
import qualified Data.Sequence as S (Seq, empty, singleton, drop, update, filter, mapWithIndex, index, findIndexL, (|>))
import qualified Data.Vector as V (Vector, toList)
import qualified Control.Monad.State as St (get, put)

import Agent

data LogItem = LogItem { timestamp :: UTCTime, level :: LogLevel, message :: String } deriving (Generic)
instance ToJSON LogItem

data LogLevel = Error | Info deriving (Generic)
instance ToJSON LogLevel

data Permission = All | Sub | Mod deriving (Generic)
instance ToJSON Permission

data Stat = StringStat { statName :: String, stringValue :: String } | IntStat { statName :: String, intValue :: Int } deriving (Generic)
instance ToJSON Stat

data Integration = PUBG { matches :: [String], stats :: S.Seq Stat } deriving (Generic)
instance ToJSON Integration

data Moderation = AccountAge Int | Blacklist (S.Seq BlacklistEntry) | Links [String] | MessageLength Int | Caps (Int,Int) | Symbols (Int,Int) | Color Bool | Display Bool deriving (Generic)
instance ToJSON Moderation

data BlacklistEntry = BlacklistEntry { regexp :: String, exempt :: Bool } deriving (Generic)
instance ToJSON BlacklistEntry
instance FromJSON BlacklistEntry

data Greeting = Greeting { gnick :: String, hours :: Int64, gmessage :: String, gposted :: Maybe UTCTime } deriving (Generic)
instance ToJSON Greeting

data Giveaway = Giveaway { prize :: String, keyword :: String, users :: [String], winners :: Int } deriving (Generic)
instance ToJSON Giveaway

data Event = BitEvent { ename :: String, bits :: Int, bitkeys :: [Word32] } deriving (Generic)
instance ToJSON Event

data Command = Command { cmdName :: String, cmdContent :: String, permLevel :: Permission, argPermLevel :: Permission, aliases :: [String], lastRun :: Maybe UTCTime } deriving (Generic)
instance ToJSON Command

data NoticeGroup = NoticeGroup { groupName :: String, interval :: Int64, ready :: Bool, notices :: S.Seq Command } deriving (Generic)
instance ToJSON NoticeGroup

data ChannelData = ChannelData { name :: String, chanID :: String, webAuth :: String, online :: Bool, commands :: S.Seq Command, settings :: S.Seq (String, String), counters :: S.Seq (String,Int), phrases :: S.Seq (String,String), noticeGroups :: S.Seq NoticeGroup, permitted :: [String], events :: S.Seq Event, subscribers :: [String], moderators :: [String], moderation :: S.Seq Moderation, greetings :: S.Seq Greeting, giveaways :: S.Seq Giveaway, recentHosts :: [String], integrations :: S.Seq Integration, commercials :: [(UTCTime, Int)], multi :: [String], agentActions :: S.Seq Action, logs :: S.Seq LogItem } deriving (Generic)
instance ToJSON ChannelData

data BabbleState = BabbleState { channelData :: S.Seq ChannelData } deriving (Generic)
instance ToJSON BabbleState

emptyState = BabbleState { channelData = S.empty }

initCom :: String -> String -> Maybe String -> Update BabbleState ()
initCom chan cid admin' = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing ->
      let data' = ChannelData { name = chan, chanID = cid, webAuth = "", online = False, commands = S.empty, settings = S.empty, counters = S.empty, phrases = S.empty, noticeGroups = S.empty, permitted = [], events = S.empty, subscribers = [], moderation = S.empty, greetings = S.empty, giveaways = S.empty, recentHosts = [], integrations = S.empty, commercials = [], multi = [], agentActions = S.empty, logs = S.empty }
      in case admin' of
           Nothing -> let newData = data' { moderators = [] } in St.put $ b { channelData = channelData S.|> newData }
           Just admin -> let newData = data' { moderators = [admin] } in St.put $ b { channelData = channelData S.|> newData }
    Just i -> pure ()
  where
    chanFilter c = name c == chan

fetchChannel :: String -> Query BabbleState (Maybe ChannelData)
fetchChannel chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i  -> pure $ Just $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchSetting :: String -> String -> Maybe String -> Query BabbleState (Maybe String)
fetchSetting chan key def = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i ->
      let settings' = settings $ S.index (channelData state) i
      in case S.findIndexL settingFilter settings' of
           Just i -> pure $ Just $ snd $ S.index settings' i
           Nothing -> pure def
  where
    chanFilter c = name c == chan
    settingFilter (k,_) = k == key

updateSetting :: String -> String -> String -> Update BabbleState ()
updateSetting chan key value = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'     = S.index channelData i
          settings' = S.filter delFilter $ settings data'
          newData   = data' { settings = settings' S.|> (key,value) }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter (k,_) = k /= key

removeSetting :: String -> String -> Update BabbleState ()
removeSetting chan key = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'     = S.index channelData i
          newData   = data' { settings = S.filter delFilter $ settings data' }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter (k,_) = k /= key

setWebAuth :: String -> String -> Update BabbleState ()
setWebAuth chan auth = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'     = S.index channelData i
          newData   = data' { webAuth = auth }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

updatePhrase :: String -> String -> String -> Update BabbleState ()
updatePhrase chan key value = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'     = S.index channelData i
          phrases' = S.filter delFilter $ phrases data'
          newData   = data' { phrases = phrases' S.|> (key,value) }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter (k,_) = k /= key

updateCounter :: String -> String -> Int -> Update BabbleState ()
updateCounter chan key value = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'     = S.index channelData i
          counters' = S.filter delFilter $ counters data'
          newData   = data' { counters = counters' S.|> (key,value) }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter (k,_) = k /= key

incCounter :: String -> String -> Update BabbleState ()
incCounter chan key = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'      = S.index channelData i
          newCounter = case S.findIndexL counterFilter (counters data') of
                         Nothing -> (key,1)
                         Just i' -> let (k,v) = S.index (counters data') i' in (k,v+1)
          counters' = S.filter (not . counterFilter) $ counters data'
          newData   = data' { counters = counters' S.|> newCounter }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    counterFilter (k,_) = k == key

fetchCounters :: String -> Query BabbleState (S.Seq (String,Int))
fetchCounters chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ counters $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchPhrases :: String -> Query BabbleState (S.Seq (String,String))
fetchPhrases chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ phrases $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchWebAuth :: String -> Query BabbleState String
fetchWebAuth chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure ""
    Just i  -> pure $ webAuth $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

addMod :: String -> String -> Update BabbleState ()
addMod chan nick = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { moderators = nick : (moderators data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

fetchMods :: String -> Query BabbleState [String]
fetchMods chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i  -> pure $ moderators $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchSubs :: String -> Query BabbleState [String]
fetchSubs chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i  -> pure $ subscribers $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchPermits :: String -> Query BabbleState [String]
fetchPermits chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i  -> pure $ permitted $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchSettings :: String -> Query BabbleState (S.Seq (String,String))
fetchSettings chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ settings $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

addSub :: String -> String -> Update BabbleState ()
addSub chan sub = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { subscribers = sub : (subscribers data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

removeSubs :: String -> Update BabbleState ()
removeSubs chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { subscribers = [] }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

permit :: String -> String -> Update BabbleState ()
permit chan nick = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { permitted = nick : (permitted data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

unpermit :: String -> String -> Update BabbleState ()
unpermit chan nick = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { permitted = filter permFilter (permitted data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    permFilter n = n /= nick

unmod :: String -> String -> Update BabbleState ()
unmod chan nick = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { moderators = L.filter delFilter (moderators data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter n = n /= nick

addHost :: String -> String -> Update BabbleState ()
addHost chan nick = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { recentHosts = nick : (recentHosts data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

resetHosts :: String -> Update BabbleState ()
resetHosts chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { recentHosts = [] }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

fetchHosts :: String -> Query BabbleState [String]
fetchHosts chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i  -> pure $ recentHosts $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchNoticeGroup :: String -> String -> Query BabbleState (Maybe NoticeGroup)
fetchNoticeGroup chan gname = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i  ->
      let groups = noticeGroups $ S.index (channelData state) i
      in case S.findIndexL groupFilter groups of
           Nothing -> pure Nothing
           Just g  -> pure $ Just $ S.index groups g
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == gname

fetchNoticeGroups :: String -> Query BabbleState (S.Seq NoticeGroup)
fetchNoticeGroups chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ noticeGroups $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchNotices :: String -> String -> Query BabbleState (S.Seq Command)
fetchNotices chan key = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i ->
      let groups = noticeGroups $ S.index (channelData state) i
      in case S.findIndexL groupFilter groups of
           Nothing -> pure S.empty
           Just g -> pure $ notices $ S.index groups g
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == key

readyGroup :: String -> String -> Update BabbleState ()
readyGroup chan key = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'  = S.index channelData i
          groups = noticeGroups data'
      in case S.findIndexL groupFilter groups of
           Nothing -> pure ()
           Just g ->
             let group     = S.index groups g
                 newGroup  = group { ready = True }
                 newData   = data' { noticeGroups = S.update g newGroup groups }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == key

unreadyGroup :: String -> String -> Bool -> Update BabbleState ()
unreadyGroup chan key rotate = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'  = S.index channelData i
          groups = noticeGroups data'
      in case S.findIndexL groupFilter groups of
           Nothing -> pure ()
           Just g ->
             let group     = S.index groups g
                 notices'  = if rotate then S.drop 1 (notices group) S.|> S.index (notices group) 0 else notices group
                 newGroup  = group { ready = False, notices = notices' }
                 newData   = data' { noticeGroups = S.update g newGroup groups }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == key

channelID :: String -> Query BabbleState String
channelID chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure ""
    Just i  -> pure $ chanID $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

status :: String -> Query BabbleState Bool
status chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure $ False
    Just i  -> pure $ online $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

updateStatus :: String -> Bool -> Update BabbleState ()
updateStatus chan on = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { online = on }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

fetchCommands :: String -> Query BabbleState (S.Seq Command)
fetchCommands chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure $ S.empty
    Just i  -> pure $ commands $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

runCom :: String -> String -> UTCTime -> Update BabbleState ()
runCom chan cname now = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data' = S.index channelData i
      in case S.findIndexL cmdFilter (commands data') of
           Nothing -> pure ()
           Just i' ->
             let cmd = S.index (commands data') i'
                 newCmd = cmd { lastRun = Just now }
                 newData = data' { commands = S.update i' newCmd (commands data') }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    cmdFilter c = cmdName c == cname

addCom :: String -> String -> String -> Permission -> Update BabbleState ()
addCom chan cname content perm = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newCmd  = case S.findIndexL cmdFilter (commands data') of
                      Nothing -> Command { cmdName = map toLower cname, cmdContent = content, permLevel = perm, argPermLevel = perm, aliases = [], lastRun = Nothing }
                      Just i' -> let cmd = S.index (commands data') i' in cmd { cmdName = map toLower cname, cmdContent = content, permLevel = perm, argPermLevel = perm }
          newData = data' { commands = S.filter (not . cmdFilter) (commands data') S.|> newCmd }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    cmdFilter c = cmdName c == cname

addAlias :: String -> String -> String -> Update BabbleState ()
addAlias chan cname alias = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data' = S.index channelData i
      in case S.findIndexL cmdFilter (commands data') of
           Nothing -> pure ()
           Just i' ->
             let cmd     = S.index (commands data') i'
                 newCmd  = cmd { aliases = L.filter (\f -> f /= alias) (aliases cmd) ++ [alias] }
                 newData = data' { commands = S.filter (not . cmdFilter) (commands data') S.|> newCmd }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    cmdFilter c = cmdName c == cname

delAlias :: String -> String -> Update BabbleState ()
delAlias chan alias = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data' = S.index channelData i
          newData = data' { commands = S.mapWithIndex cmdFilter (commands data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    cmdFilter i cmd = cmd { aliases = L.filter (\a -> a /= alias) (aliases cmd) }

fetchAlias :: String -> String -> Query BabbleState [String]
fetchAlias chan cname = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i ->
      let data' = S.index (channelData state) i
      in case S.findIndexL cmdFilter (commands data') of
           Nothing -> pure []
           Just i' -> pure $ aliases $ S.index (commands data') i'
  where
    chanFilter c = name c == chan
    cmdFilter c = cmdName c == cname

delCom :: String -> String -> Update BabbleState ()
delCom chan cname = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { commands = S.filter delFilter (commands data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter c = cmdName c /= cname

addNoticeGroup :: String -> String -> Int64 -> Update BabbleState ()
addNoticeGroup chan gname ginterval = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          groups  = S.filter delFilter $ noticeGroups data'
          group   = NoticeGroup { groupName = gname, interval = ginterval, ready = False, notices = S.empty }
          newData = data' { noticeGroups = groups S.|> group }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter g = groupName g /= gname

editNoticeGroup :: String -> String -> String -> Int64 -> Update BabbleState ()
editNoticeGroup chan gname newname ginterval = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data' = S.index channelData i
      in case S.findIndexL groupFilter (noticeGroups data') of
           Nothing -> pure ()
           Just i' ->
             let group = S.index (noticeGroups data') i'
                 groups = S.filter (not. groupFilter) (noticeGroups data')
                 newGroup = group { groupName = gname, interval = ginterval }
                 newData = data' { noticeGroups = groups S.|> group }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == gname

delNoticeGroup :: String -> String -> Update BabbleState ()
delNoticeGroup chan gname = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { noticeGroups = S.filter delFilter $ noticeGroups data' }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter g = groupName g /= gname

addNotice :: String -> String -> Command -> Update BabbleState ()
addNotice chan gname cmd = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'  = S.index channelData i
          groups = noticeGroups data'
      in case S.findIndexL groupFilter groups of
           Nothing -> pure ()
           Just g ->
             let group     = S.index groups g
                 newGroup  = group { notices = S.filter delFilter (notices group) S.|> cmd }
                 newData   = data' { noticeGroups = S.update g newGroup groups }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == gname
    delFilter c = cmdName c /= cmdName cmd

delNotice :: String -> String -> String -> Update BabbleState ()
delNotice chan gname cmd = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'  = S.index channelData i
          groups = noticeGroups data'
      in case S.findIndexL groupFilter groups of
           Nothing -> pure ()
           Just g ->
             let group     = S.index groups g
                 newGroup  = group { notices = S.filter delFilter (notices group) }
                 newData   = data' { noticeGroups = S.update g newGroup groups }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    groupFilter g = groupName g == gname
    delFilter c = cmdName c /= cmd

fetchMulti :: String -> Query BabbleState [String]
fetchMulti chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i  -> pure $ multi $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

updateMulti :: String -> String -> [String] -> Update BabbleState ()
updateMulti chan cmd streams = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
      in case cmd of
           "clear" -> let newData = data' { multi = [] } in St.put $ b { channelData = S.update i newData channelData }
           "set"   -> let newData = data' { multi = streams } in St.put $ b { channelData = S.update i newData channelData }
           _ -> pure ()
  where
    chanFilter c = name c == chan
    delFilter (cmd,_,_) = cmd /= cmd

setGreeting :: String -> String -> Int64 -> String -> Update BabbleState ()
setGreeting chan nick hours message = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data' = S.index channelData i
          newData = case S.findIndexL greetingFilter (greetings data') of
                      Nothing -> let newGreeting = Greeting { gnick = nick, hours = hours, gmessage = message, gposted = Nothing }
                                 in  data' { greetings = (greetings data') S.|> newGreeting }
                      Just i' -> let greeting    = S.index (greetings data') i'
                                     newGreeting = greeting { hours = hours, gmessage = message }
                                 in  data' { greetings = S.update i' newGreeting (greetings data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    greetingFilter g = gnick g == nick

postGreeting :: String -> Greeting -> UTCTime -> Update BabbleState ()
postGreeting chan greeting now = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data' = S.index channelData i
      in case S.findIndexL greetingFilter (greetings data') of
           Nothing -> pure ()
           Just i' ->
             let greeting'   = S.index (greetings data') i'
                 newGreeting = greeting' { gposted = Just now }
                 newData     = data' { greetings = S.update i' newGreeting (greetings data') }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    greetingFilter g = gnick g == gnick greeting

fetchGreeting :: String -> String -> Query BabbleState (Maybe Greeting)
fetchGreeting chan nick = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i  -> let data' = S.index (channelData state) i
               in case S.findIndexL greetingFilter (greetings data') of
                    Nothing -> pure Nothing
                    Just i' -> pure $ Just $ S.index (greetings data') i'
  where
    chanFilter c = name c == chan
    greetingFilter g = gnick g == nick

fetchGiveaways :: String -> Query BabbleState (S.Seq Giveaway)
fetchGiveaways chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ giveaways $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

fetchGiveaway :: String -> String -> Query BabbleState (Maybe Giveaway)
fetchGiveaway chan gname = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i ->
      let gaways = giveaways $ S.index (channelData state) i
      in case S.findIndexL giveFilter gaways of
           Nothing -> pure Nothing
           Just g -> pure $ Just $ S.index gaways g
  where
    chanFilter c = name c == chan
    giveFilter g = prize g == gname

addToGiveaway :: String -> String -> String -> Update BabbleState ()
addToGiveaway chan gname nick = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          gaways  = giveaways data'
      in case S.findIndexL giveFilter gaways of
           Nothing -> pure ()
           Just g  ->
             let giveaway = S.index gaways g
                 newG     = giveaway { users = if elem nick (users giveaway) then users giveaway else nick : (users giveaway) }
                 newData  = data' { giveaways = S.update g newG gaways }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    giveFilter g = prize g == gname

startGiveaway :: String -> String -> String -> Int -> Update BabbleState ()
startGiveaway chan gname key num = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          gaways  = S.filter delFilter $ giveaways data'
          newData = data' { giveaways = gaways S.|> Giveaway { prize = gname, keyword = key, winners = num, users = [] } }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter g = prize g /= gname

endGiveaway :: String -> String -> Update BabbleState ()
endGiveaway chan gname = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { giveaways = S.filter delFilter $ giveaways data' }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter g = prize g /= gname

fetchCommercials :: String -> Query BabbleState [(UTCTime, Int)]
fetchCommercials chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i  -> pure $ commercials $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

addCommercial :: String -> UTCTime -> Int -> Update BabbleState ()
addCommercial chan time num = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          commercials' = commercials data'
          newComs = if length commercials' > 7 then drop (length commercials' - 7) commercials' else commercials'
          newData = data' { commercials = newComs ++ [(time,num)] }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

addBlacklist :: String -> String -> Bool -> Update BabbleState ()
addBlacklist chan fcontent exempt = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          moderation'  = moderation data'
          newList = case S.findIndexL modFilter moderation' of
                      Nothing -> Blacklist $ S.singleton $ BlacklistEntry fcontent exempt
                      Just i' -> let Blacklist filters = S.index moderation' i' in Blacklist $ S.filter delFilter filters S.|> BlacklistEntry fcontent exempt
          newData   = data' { moderation = S.filter (not . modFilter) moderation' S.|> newList }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter e = regexp e /= fcontent
    modFilter m = case m of
      Blacklist {} -> True
      _ -> False

delBlacklist :: String -> String -> Update BabbleState ()
delBlacklist chan fcontent = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          moderation'  = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure ()
           Just i' -> let newList = let Blacklist filters = S.index moderation' i' in Blacklist $ S.filter delFilter filters
                          newData = data' { moderation = S.update i' newList moderation' }
                      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter entry = regexp entry /= fcontent
    modFilter m = case m of
      Blacklist {} -> True
      _ -> False

fetchBlacklist :: String -> Query BabbleState (S.Seq BlacklistEntry)
fetchBlacklist chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure $ S.empty
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure $ S.empty
           Just i' -> let Blacklist filters = S.index moderation' i' in pure filters
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Blacklist {} -> True
      _ -> False

setCapFilter :: String -> Int -> Int -> Update BabbleState ()
setCapFilter chan per trigger = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData   = data' { moderation = S.filter modFilter (moderation data') S.|> Caps (per,trigger) }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Caps {} -> False
      _ -> True

removeCapFilter :: String -> Update BabbleState ()
removeCapFilter chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData   = data' { moderation = S.filter modFilter (moderation data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Caps {} -> False
      _ -> True

setSymFilter :: String -> Int -> Int -> Update BabbleState ()
setSymFilter chan per trigger = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData   = data' { moderation = S.filter modFilter (moderation data') S.|> Symbols (per,trigger) }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Symbols {} -> False
      _ -> True

removeSymFilter :: String -> Update BabbleState ()
removeSymFilter chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData   = data' { moderation = S.filter modFilter (moderation data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Symbols {} -> False
      _ -> True

fetchSymFilter :: String -> Query BabbleState (Maybe (Int,Int))
fetchSymFilter chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure Nothing
           Just i' -> let Symbols syms = S.index moderation' i' in pure $ Just syms
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Symbols {} -> True
      _ -> False

setLength :: String -> Int -> Update BabbleState ()
setLength chan len = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData   = data' { moderation = S.filter modFilter (moderation data') S.|> MessageLength len }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      MessageLength {} -> False
      _ -> True

removeLength :: String -> Update BabbleState ()
removeLength chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData   = data' { moderation = S.filter modFilter (moderation data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      MessageLength {} -> False
      _ -> True

fetchColorFilter :: String -> Query BabbleState Bool
fetchColorFilter chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure False
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure False
           Just i' -> let Color bool = S.index moderation' i' in pure bool
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Color {} -> True
      _ -> False

setColorFilter :: String -> Bool -> Update BabbleState ()
setColorFilter chan bool = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { moderation = S.filter modFilter (moderation data') S.|> Color bool }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Color {} -> False
      _ -> True

setDisplayModeration :: String -> Bool -> Update BabbleState ()
setDisplayModeration chan bool = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { moderation = S.filter modFilter (moderation data') S.|> Display bool }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Display {} -> False
      _ -> True

fetchDisplayModeration :: String -> Query BabbleState Bool
fetchDisplayModeration chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure False
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure False
           Just i' -> let Display bool = S.index moderation' i' in pure bool
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Display {} -> True
      _ -> False

fetchCapFilter :: String -> Query BabbleState (Maybe (Int,Int))
fetchCapFilter chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure Nothing
           Just i' -> let Caps caps = S.index moderation' i' in pure $ Just caps
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Caps {} -> True
      _ -> False

fetchLength :: String -> Query BabbleState (Maybe Int)
fetchLength chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure Nothing
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure Nothing
           Just i' -> let MessageLength len = S.index moderation' i' in pure $ Just len
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      MessageLength {} -> True
      _ -> False

addLink :: String -> String -> Update BabbleState ()
addLink chan link = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          moderation'  = moderation data'
          newLinks = case S.findIndexL modFilter moderation' of
                      Nothing -> Links [link]
                      Just i' -> let Links links = S.index moderation' i' in Links $ link : L.filter delFilter links
          newData   = data' { moderation = S.filter (not . modFilter) moderation' S.|> newLinks }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter l = l /= link
    modFilter m = case m of
      Links {} -> True
      _ -> False

delLink :: String -> String -> Update BabbleState ()
delLink chan link = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          moderation'  = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure ()
           Just i' ->
             let newLinks = let Links links = S.index moderation' i' in Links $ L.filter delFilter links
                 newData   = data' { moderation = S.filter (not . modFilter) moderation' S.|> newLinks }
             in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    delFilter l = l /= link
    modFilter m = case m of
      Links {} -> True
      _ -> False

fetchLinks :: String -> Query BabbleState [String]
fetchLinks chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i ->
      let data' = S.index (channelData state) i
          moderation' = moderation data'
      in case S.findIndexL modFilter moderation' of
           Nothing -> pure []
           Just i' -> let Links links = S.index moderation' i' in pure links
  where
    chanFilter c = name c == chan
    modFilter m = case m of
      Links {} -> True
      _ -> False

addLogMessage :: String -> UTCTime -> LogLevel -> String -> Update BabbleState ()
addLogMessage chan time lvl msg = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newLog  = LogItem { timestamp = time, level = lvl, message = msg }
          newData = data' { logs = (logs data') S.|> newLog }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

addBitEvent :: String -> String -> Int -> [Word32] -> Update BabbleState ()
addBitEvent chan ename bits keys = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          event   = BitEvent { ename = ename, bits = bits, bitkeys = keys }
          newData = data' { events = (events data') S.|> event }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

removeBitEvent :: String -> String -> Update BabbleState ()
removeBitEvent chan name' = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { events = S.filter eventFilter (events data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    eventFilter e = ename e /= name'

fetchBitEvents :: String -> Query BabbleState (S.Seq Event)
fetchBitEvents chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ events $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

addAction :: String -> Action -> Update BabbleState ()
addAction chan action = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { agentActions = (agentActions data') S.|> action }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

fetchActions :: String -> Query BabbleState (S.Seq Action)
fetchActions chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ agentActions $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

removeActions :: String -> Update BabbleState ()
removeActions chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { agentActions = S.empty }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

fetchLogs :: String -> Query BabbleState (S.Seq LogItem)
fetchLogs chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure S.empty
    Just i  -> pure $ logs $ S.index (channelData state) i
  where
    chanFilter c = name c == chan

removeLogs :: String -> Update BabbleState ()
removeLogs chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { logs = S.empty }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan

fetchPubgMatches :: String -> Query BabbleState [String]
fetchPubgMatches chan = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure []
    Just i ->
      let data' = S.index (channelData state) i
          integrations' = integrations data'
      in case S.findIndexL intFilter integrations' of
           Nothing -> pure []
           Just i -> pure $ matches $ S.index integrations' i
  where
    chanFilter c = name c == chan
    intFilter i = case i of
      PUBG {} -> True
      _ -> False

updatePubgMatches :: String -> [String] -> Update BabbleState ()
updatePubgMatches chan newMatches = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'  = S.index channelData i
          newInt = case S.findIndexL intFilter $ integrations data' of
                     Nothing -> PUBG { matches = newMatches, stats = S.empty }
                     Just i' -> let pubg = S.index (integrations data') i' in pubg { matches = (matches pubg) ++ newMatches }
          newData = data' { integrations = S.filter (not . intFilter) (integrations data') S.|> newInt }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    intFilter i = case i of
      PUBG {} -> True
      _ -> False

updatePubgStat :: String -> Stat -> Update BabbleState ()
updatePubgStat chan stat = do
  if statName stat /= "winPlace" || statName stat == "winPlace" && intValue stat == 1 then do
    b@BabbleState{..} <- St.get
    case S.findIndexL chanFilter channelData of
      Nothing -> pure ()
      Just i ->
        let data' = S.index channelData i in
        case S.findIndexL intFilter $ integrations data' of
          Nothing -> pure ()
          Just i'->
            let integration = S.index (integrations data') i'
                newInt = case S.findIndexL statFilter (stats integration) of
                           Nothing -> integration { stats = S.filter (not . statFilter) (stats integration) S.|> stat }
                           Just s  ->
                             let prevStat = S.index (stats integration) s
                             in integration { stats = S.filter (not . statFilter) (stats integration) S.|> stat { intValue = (intValue prevStat) + (intValue stat) } }
                newData = data' { integrations = S.filter (not . intFilter) (integrations data') S.|> newInt }
            in St.put $ b { channelData = S.update i newData channelData }
  else pure ()
  where
    chanFilter c = name c == chan
    intFilter i = case i of
      PUBG {} -> True
      _ -> False
    statFilter stat' = statName stat' == statName stat

fetchPubgStat :: String -> String -> Query BabbleState Int
fetchPubgStat chan st = do
  state <- ask
  case S.findIndexL chanFilter (channelData state) of
    Nothing -> pure 0
    Just i ->
      let data' = S.index (channelData state) i
          integrations' = integrations data'
      in case S.findIndexL intFilter integrations' of
           Nothing -> pure 0
           Just i' ->
             let integration = S.index (integrations data') i'
             in case S.findIndexL statFilter (stats integration) of
               Nothing -> pure 0
               Just s -> pure $ intValue $ S.index (stats integration) s
  where
    chanFilter c = name c == chan
    statFilter stat' = statName stat' == st
    intFilter i = case i of
      PUBG {} -> True
      _ -> False

resetPubg :: String -> Update BabbleState ()
resetPubg chan = do
  b@BabbleState{..} <- St.get
  case S.findIndexL chanFilter channelData of
    Nothing -> pure ()
    Just i ->
      let data'   = S.index channelData i
          newData = data' { integrations = S.filter intFilter (integrations data') }
      in St.put $ b { channelData = S.update i newData channelData }
  where
    chanFilter c = name c == chan
    intFilter i = case i of
      PUBG {} -> False
      _ -> True


$(makeAcidic ''BabbleState ['initCom, 'setWebAuth, 'fetchWebAuth, 'fetchChannel, 'fetchBitEvents, 'addBitEvent, 'removeBitEvent, 'fetchSubs, 'fetchPermits, 'permit, 'unpermit, 'fetchSetting, 'updateSetting, 'removeSetting, 'fetchPhrases, 'updatePhrase, 'updateCounter, 'incCounter, 'fetchCounters, 'fetchMods, 'fetchSettings, 'addSub, 'removeSubs, 'unmod, 'addMod, 'addHost, 'resetHosts, 'fetchHosts, 'channelID, 'fetchNoticeGroup, 'fetchNoticeGroups, 'fetchNotices, 'fetchGiveaway, 'fetchGiveaways, 'startGiveaway, 'endGiveaway, 'addToGiveaway, 'readyGroup, 'unreadyGroup, 'status, 'updateStatus, 'fetchCommands, 'runCom, 'addCom, 'addAlias, 'delAlias, 'fetchAlias, 'delCom, 'addNoticeGroup, 'editNoticeGroup, 'delNoticeGroup, 'addNotice, 'delNotice, 'setGreeting, 'postGreeting, 'fetchGreeting, 'updateMulti, 'fetchMulti, 'fetchCommercials, 'addCommercial, 'addBlacklist, 'delBlacklist, 'fetchBlacklist, 'fetchLinks, 'addLink, 'delLink, 'fetchLength, 'setLength, 'removeLength, 'fetchColorFilter, 'setColorFilter, 'setDisplayModeration, 'fetchDisplayModeration, 'setCapFilter, 'removeCapFilter, 'fetchCapFilter, 'setSymFilter, 'removeSymFilter, 'fetchSymFilter, 'addLogMessage, 'addAction, 'fetchActions, 'removeActions, 'fetchLogs, 'removeLogs, 'fetchPubgMatches, 'updatePubgMatches, 'updatePubgStat, 'fetchPubgStat, 'resetPubg])

$(deriveSafeCopy 8 'extension ''ChannelData)
$(deriveSafeCopy 3 'extension ''Command)
$(deriveSafeCopy 1 'extension ''Moderation)
$(deriveSafeCopy 1 'extension ''Action)
$(deriveSafeCopy 0 'base ''Event)
$(deriveSafeCopy 0 'base ''BlacklistEntry)
$(deriveSafeCopy 0 'base ''BabbleState)
$(deriveSafeCopy 0 'base ''NoticeGroup)
$(deriveSafeCopy 0 'base ''Integration)
$(deriveSafeCopy 0 'base ''Stat)
$(deriveSafeCopy 0 'base ''Permission)
$(deriveSafeCopy 0 'base ''Greeting)
$(deriveSafeCopy 0 'base ''Giveaway)
$(deriveSafeCopy 0 'base ''LogItem)
$(deriveSafeCopy 0 'base ''LogLevel)

data ChannelData_v0 = ChannelData_v0 String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq NoticeGroup) [String] [String] (S.Seq Moderation) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String]
$(deriveSafeCopy 0 'base ''ChannelData_v0)

data ChannelData_v1 = ChannelData_v1 String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq NoticeGroup) [String] [String] (S.Seq Moderation) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq LogItem)
$(deriveSafeCopy 1 'extension ''ChannelData_v1)

data ChannelData_v2 = ChannelData_v2 String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq (String,Int)) (S.Seq NoticeGroup) [String] [String] (S.Seq Moderation) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq LogItem)
$(deriveSafeCopy 2 'extension ''ChannelData_v2)

data ChannelData_v3 = ChannelData_v3 String String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq (String,Int)) (S.Seq NoticeGroup) [String] [String] (S.Seq Moderation) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq LogItem)
$(deriveSafeCopy 3 'extension ''ChannelData_v3)

data ChannelData_v4 = ChannelData_v4 String String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq (String,Int)) (S.Seq NoticeGroup) [String] [String] (S.Seq Moderation) (S.Seq Greeting) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq LogItem)
$(deriveSafeCopy 4 'extension ''ChannelData_v4)

data ChannelData_v5 = ChannelData_v5 String String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq (String,Int)) (S.Seq NoticeGroup) [String] [String] [String] (S.Seq Moderation) (S.Seq Greeting) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq LogItem)
$(deriveSafeCopy 5 'extension ''ChannelData_v5)

data ChannelData_v6 = ChannelData_v6 String String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq (String,Int)) (S.Seq NoticeGroup) [String] [String] [String] (S.Seq Moderation) (S.Seq Greeting) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq Action) (S.Seq LogItem)
$(deriveSafeCopy 6 'extension ''ChannelData_v6)

data ChannelData_v7 = ChannelData_v7 String String String Bool (S.Seq Command) (S.Seq (String, String)) (S.Seq (String,Int)) (S.Seq (String,String)) (S.Seq NoticeGroup) [String] [String] [String] (S.Seq Moderation) (S.Seq Greeting) (S.Seq Giveaway) [String] (S.Seq Integration) [(UTCTime, Int)] [String] (S.Seq Action) (S.Seq LogItem)
$(deriveSafeCopy 7 'extension ''ChannelData_v7)

data Command_v0 = Command_v0 String String Permission Permission
$(deriveSafeCopy 0 'base ''Command_v0)

data Command_v1 = Command_v1 String String Permission Permission | Alias_v1 String Command
$(deriveSafeCopy 1 'extension ''Command_v1)

data Command_v2 = Command_v2 String String Permission Permission [String]
$(deriveSafeCopy 2 'extension ''Command_v2)

data Moderation_v0 = AccountAge_v0 Int | Blacklist_v0 (S.Seq String) | Links_v0 [String] | MessageLength_v0 Int | Caps_v0 (Int,Int) | Symbols_v0 (Int,Int) | Color_v0 Bool | Display_v0 Bool deriving (Generic)
$(deriveSafeCopy 0 'base ''Moderation_v0)

data Action_v0 = Update_v0 { version :: String } | Input_v0 { key :: Word32 } | SceneChange_v0 { sceneName :: String }
$(deriveSafeCopy 0 'base ''Action_v0)

instance Migrate ChannelData_v1 where
  type MigrateFrom ChannelData_v1 = ChannelData_v0
  migrate (ChannelData_v0 name' chanID' online' commands' settings' noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi') = ChannelData_v1 name' chanID' online' commands' settings' noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi' S.empty

instance Migrate ChannelData_v2 where
  type MigrateFrom ChannelData_v2 = ChannelData_v1
  migrate (ChannelData_v1 name' chanID' online' commands' settings' noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi' logs') = ChannelData_v2 name' chanID' online' commands' settings' S.empty noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi' logs'

instance Migrate ChannelData_v3 where
  type MigrateFrom ChannelData_v3 = ChannelData_v2
  migrate (ChannelData_v2 name' chanID' online' commands' settings' counters' noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi' logs') = ChannelData_v3 name' chanID' "" online' commands' settings' counters' noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi' logs'

instance Migrate ChannelData_v4 where
  type MigrateFrom ChannelData_v4 = ChannelData_v3
  migrate (ChannelData_v3 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' subscribers' moderators' moderation' giveaways' recentHosts' integrations' commercials' multi' logs') = ChannelData_v4 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' subscribers' moderators' moderation' S.empty giveaways' recentHosts' integrations' commercials' multi' logs'

instance Migrate ChannelData_v5 where
  type MigrateFrom ChannelData_v5 = ChannelData_v4
  migrate (ChannelData_v4 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' logs') = ChannelData_v5 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' [] subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' logs'

instance Migrate ChannelData_v6 where
  type MigrateFrom ChannelData_v6 = ChannelData_v5
  migrate (ChannelData_v5 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' permitted' subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' logs') = ChannelData_v6 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' permitted' subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' S.empty logs'

instance Migrate ChannelData_v7 where
  type MigrateFrom ChannelData_v7 = ChannelData_v6
  migrate (ChannelData_v6 name' chanID' webAuth' online' commands' settings' counters' noticeGroups' permitted' subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' actions' logs') = ChannelData_v7 name' chanID' webAuth' online' commands' settings' counters' S.empty noticeGroups' permitted' subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' actions' logs'

instance Migrate ChannelData where
  type MigrateFrom ChannelData = ChannelData_v7
  migrate (ChannelData_v7 name' chanID' webAuth' online' commands' settings' counters' phrases' noticeGroups' permitted' subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' actions' logs') = ChannelData name' chanID' webAuth' online' commands' settings' counters' phrases' noticeGroups' permitted' S.empty subscribers' moderators' moderation' greetings' giveaways' recentHosts' integrations' commercials' multi' actions' logs'

instance Migrate Command_v1 where
  type MigrateFrom Command_v1 = Command_v0
  migrate (Command_v0 name' content' perm' argPerm') = Command_v1 name' content' perm' argPerm'

instance Migrate Command_v2 where
  type MigrateFrom Command_v2 = Command_v1
  migrate (Command_v1 name' content' perm' argPerm') = Command_v2 name' content' perm' argPerm' []
  migrate (Alias_v1 name' cmd') = Command_v2 name' "" All All []

instance Migrate Command where
  type MigrateFrom Command = Command_v2
  migrate (Command_v2 name' content' perm' argPerm' aliases') = Command name' content' perm' argPerm' aliases' Nothing

instance Migrate Action where
  type MigrateFrom Action = Action_v0
  migrate (Update_v0 v) = Update v
  migrate (SceneChange_v0 s) = SceneChange s
  migrate (Input_v0 k) = Input [k]

instance Migrate Moderation where
  type MigrateFrom Moderation = Moderation_v0
  migrate (AccountAge_v0 int') = AccountAge int'
  migrate (Blacklist_v0 strings') = Blacklist (S.mapWithIndex (\i s -> BlacklistEntry s False) strings')
  migrate (Links_v0 strings') = Links strings'
  migrate (MessageLength_v0 int') = MessageLength int'
  migrate (Caps_v0 (int1,int2)) = Caps (int1,int2)
  migrate (Symbols_v0 (int1,int2)) = Symbols (int1,int2)
  migrate (Color_v0 bool') = Color bool'
  migrate (Display_v0 bool') = Display bool'
