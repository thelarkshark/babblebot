{-# LANGUAGE OverloadedStrings, FlexibleContexts, DeriveGeneric #-}

module Lib
  ( babblebot
  ) where

import GHC.Generics (Generic)
import GHC.Int (Int64)
import Data.Char (toLower)
import Data.Aeson (Value(..), FromJSON, encode, decode, object)
import Data.Acid (AcidState, makeAcidic, query, update, openLocalState)
import Data.Acid.Local (createCheckpointAndClose)
import Data.Text.Foreign (dropWord16, takeWord16)
import Data.Text.ICU.Regex (MatchOption(..), regex, find, setText, clone, groupCount, start_, end_)
import Data.Time.Clock (diffUTCTime, getCurrentTime)
import Data.UUID.V4
import Data.List.Split (wordsBy)
import System.IO (getLine, hGetLine, hPutStrLn, hSetBuffering, hClose, stdout, BufferMode(..))
import System.Exit (exitSuccess)
import System.Posix.Signals (sigTERM, installHandler, Handler(..))
import Control.Exception (bracket)
import Control.Lens ((^.), (.~), (&), (%~))
import Control.Monad (msum, foldM, forever)
import Control.Monad.IO.Class (liftIO)
import Control.Concurrent (forkIO, myThreadId, killThread, threadDelay)
import Control.Concurrent.Async (async, wait, waitAny)
import Control.Concurrent.Timer (repeatedTimer, oneShotTimer)
import Control.Concurrent.Suspend (mDelay, sDelay, msDelay)
import Control.Concurrent.STM.TVar (readTVarIO)
import Network
import Network.IRC.Client
import Network.IRC.CTCP
import Happstack.Server
import qualified Network.Wreq as W
import qualified Network.URI as U (parseURI, uriAuthority, uriRegName, uriPath)
import qualified Network.WebSockets as WS (ConnectionException, sendClose, sendTextData, receiveData)
import qualified Wuss as WU (runSecureClient)
import qualified Data.UUID as UUID
import qualified Data.Serialize as SE (encode, decode)
import qualified Data.Map.Strict as M (singleton)
import qualified Data.HashMap.Strict as HM (lookup)
import qualified Data.Sequence as S (Seq, filter, index, singleton, empty)
import qualified Data.Foldable as F (toList)
import qualified Data.List as L (intercalate, concat, sort, find, span, partition, isInfixOf)
import qualified Data.Yaml as Y (ParseException, decodeFileEither, (.=))
import qualified Data.Text as T (Text, pack, unpack)
import qualified Data.Vector as V (head, any, fromList, toList)
import qualified Data.ByteString as B (ByteString)
import qualified Data.ByteString.Char8 as C (pack, unpack)
import qualified Data.ByteString.Lazy as LB (ByteString)
import qualified Data.ByteString.Lazy.Char8 as LBC (pack, unpack)
import qualified Control.Exception as E
import Text.Blaze.Html5 (Html, (!), a, form, input, p, toHtml, label)
import Text.Blaze.Html5.Attributes (action, enctype, href, name, size, type_, value)
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Html5.Attributes as A

import State
import Commands
import Config
import Timers
import Util
import Agent

agentVersion = "v0.2.3.0"

babblebot = do
  hSetBuffering stdout LineBuffering
  tid <- myThreadId
  installHandler sigTERM (Catch $ killThread tid) Nothing
  parsedContent <- Y.decodeFileEither "config.yaml" :: IO (Either Y.ParseException Config)
  case parsedContent of
    Left e -> error (show e)
    (Right (Config admin' agent' bots)) -> do
        bracket (openLocalState emptyState)
                (createCheckpointAndClose) $
                \acid -> do
                  botInfo <- sequence $ map (\b -> createIRCState acid b (Config.channels b)) bots
                  mapM (\(b,cs,irc) -> mapM (botInit acid admin' irc b) cs) botInfo
                  let webHandles = L.concat $ map (\(b,cs,irc) -> map (\c -> async $ runWeb acid b c) cs) botInfo
                      discordHandles = map (\(b,cs,irc) -> async $ runDiscord acid b cs) botInfo
                      ircHandles = map (\(b,cs,irc) -> async $ runIRC (bot b) irc) botInfo
                  handles <- sequence $ webHandles ++ ircHandles ++ discordHandles
                  forkIO $ parseConsole acid botInfo
                  forkIO $ pubsubListener acid bots
                  case agent' of
                    Nothing -> forkIO $ pure ()
                    Just agentC -> forkIO $ agentListener agentC acid bots
                  waitAny handles
  where
    botInit acid admin' irc b c = do
      initChan acid admin' b c
      groups <- query acid $ FetchNoticeGroups $ channel c
      mapM_ (\group -> update acid $ UnreadyGroup (channel c) (groupName group) False) groups
      mapM_ (\group -> oneShotTimer (update acid $ ReadyGroup (channel c) (groupName group)) (sDelay $ interval group)) groups
      mapM_ (\timer -> repeatedTimer (timer acid irc b c) (sDelay 60)) [statusUpdate,flushLogs]
      status <- query acid $ Status $ channel c
      if status then startTimers acid irc b c else pure ()

    createIRCState :: AcidState BabbleState -> BotConfig -> [ChannelConfig] -> IO (BotConfig, [ChannelConfig], (IRCState IrcState))
    createIRCState acid b cs = do
      let conn = tlsConnection (WithDefaultConfig "irc.chat.twitch.tv" 443) & password %~ (\_ -> (Just $ T.pack $ "oauth:" ++ passphrase b))
          cfg  = defaultInstanceConfig (T.pack $ bot b) & Network.IRC.Client.channels %~ (\_ -> map (\c -> T.pack $ "#" ++ channel c) cs) & handlers %~ (\h -> h ++ [msgHandler])
          istate = IrcState { db = acid, botConfig = b, channelConfigs = cs }
      irc <- newIRCState conn cfg istate
      pure (b, cs, irc)

    parseConsole db botInfo = do
      msg <- getLine
      let args  = words msg
      if length args < 2 then parseConsole db botInfo
      else let name' = tail $ init $ head args
           in case L.find (findBot name') botInfo of
                Nothing -> parseConsole db botInfo
                Just (b,cs,irc) -> do
                  let Just chan = L.find (\c -> channel c == name') cs
                      args' = if length args < 3 then [] else tail $ tail args
                  runIRCAction (runCmd chan "" (map toLower $ head $ tail args) args' False) irc
                  parseConsole db botInfo
      where
        findBot name' (b,cs,_) = any (\c -> channel c == name') cs

    agentListener agentC acid bots = withSocketsDo $ do
      sock <- listenOn $ PortNumber (fromIntegral $ Config.port agentC)
      handleConnections sock
      where
        handleConnections sock = do
          (handle, host, port) <- accept sock
          contents <- hGetLine handle
          let authAgent' = SE.decode $ C.pack contents
          case authAgent' of
            Left err -> hClose handle
            Right authAgent -> do
              let chans = concat $ map (\b -> map (\c -> (b,c)) (Config.channels b)) bots
                  chan' = L.find (chanFilter $ channelName authAgent) chans
              case chan' of
                Nothing -> hClose handle
                Just (b,c) -> do
                  if token authAgent /= secret agentC then hClose handle
                  else do
                    mapM_ (runAction acid c) (actions authAgent)
                    agentActions <- query acid $ FetchActions (channel c)
                    update acid $ RemoveActions $ channel c
                    let agent = Agent { actions = agentActions, channelName = channelName authAgent, token = token authAgent }
                    hPutStrLn handle (C.unpack $ SE.encode agent)
                    hClose handle
          handleConnections sock
        chanFilter cname (b,c) = cname == channel c
        runAction acid chan action = do
          case action of
            Update ver -> do
              if agentVersion == ver then pure ()
              else do
                let updateAction = Update { Agent.version = agentVersion }
                update acid $ AddAction (channel chan) updateAction
            _ -> pure ()

    pubsubListener acid bots = withSocketsDo $ do
      WU.runSecureClient "pubsub-edge.twitch.tv" 443 "/" (app token)
      where
        app token conn = do
          repeatedTimer (WS.sendTextData conn $ encode $ object [ "type" Y..= ("PING"::String) ]) (mDelay 4)
          let chans = concat $ map (\b -> map (\c -> (b,c)) (Config.channels b)) bots
          mapM_ listenReq chans
          forever $ do
            msg <- WS.receiveData conn
            let json = decode msg :: Maybe Value
            case json of
              Nothing -> pure ()
              Just (Object o) ->
                case HM.lookup "data" o of
                  Nothing -> pure ()
                  Just (Object o) ->
                    case HM.lookup "message" o of
                      Nothing -> pure ()
                      Just (String msg) -> do
                        let json = decode (LBC.pack $ T.unpack msg) :: Maybe Value
                        case json of
                          Nothing -> pure ()
                          Just (Object o) ->
                            case HM.lookup "data" o of
                              Nothing -> pure ()
                              Just (Object o) ->
                                case HM.lookup "channel_name" o of
                                  Nothing -> pure ()
                                  Just (String cname) -> do
                                    let Just (_,chan) = L.find (\(_,c) -> channel c == T.unpack cname) chans
                                    case HM.lookup "bits_used" o of
                                      Nothing -> pure ()
                                      Just (Number n) -> do
                                        events <- liftIO $ query acid $ FetchBitEvents (channel chan)
                                        mapM_ (parseEvent chan) events
          where
            parseEvent chan ev = do
              let action = Agent.Input { keys = bitkeys ev }
              update acid $ AddAction (channel chan) action
            listenReq (b,c) = do
              cid <- query acid $ ChannelID (channel c)
              WS.sendTextData conn $ encode $ object [ "type" Y..= ("LISTEN"::String), "data" Y..= object [ "topics" Y..= ["channel-bits-events-v1." ++ cid], "auth_token" Y..= oauth c ] ]

initChan :: AcidState BabbleState -> Maybe String -> BotConfig -> ChannelConfig -> IO ()
initChan state admin' b c = do
  chan <- query state $ FetchChannel (channel c)
  case chan of
    Just _  -> pure ()
    Nothing -> do
      let opts = W.defaults & W.header "Authorization" .~ [C.pack $ "Bearer " ++ oauth c]
      rsp' <- safeGetWith opts "https://api.twitch.tv/helix/users"
      case rsp' of
        Left err -> logMsg state c Error err
        Right rsp -> do
          let body = decode $ rsp ^. W.responseBody :: Maybe Value
          case body of
            Nothing -> pure ()
            Just (Object o) ->
              case HM.lookup "data" o of
                Nothing -> pure ()
                Just (Array a) ->
                  if length a == 0 then pure ()
                  else
                    case V.head a of
                      Object o ->
                        let Just (String cid) = HM.lookup "id" o
                            Just (String display) = HM.lookup "display_name" o
                        in update state (InitCom (channel c) (T.unpack cid) admin') >> update state (UpdateSetting (channel c) "channel:display-name" (T.unpack display))
                      _ -> pure ()
          update state $ UpdateSetting (channel c) "command:prefix" "!"
          auth <- nextRandom
          update state $ SetWebAuth (channel c) (UUID.toString auth)

runIRC :: String -> IRCState IrcState -> IO ()
runIRC nbot irc = runClientWith irc `E.catch` (ircEHandler nbot)
  where
    ircEHandler :: String -> E.IOException -> IO ()
    ircEHandler nbot e = do
      threadDelay 10000000
      print ("[ircEHandler: " ++ nbot ++ "]") >> runClientWith irc `E.catch` (ircEHandler nbot)

msgHandler :: EventHandler IrcState
msgHandler = EventHandler (matchType _Privmsg) $ \src (chan,msg') -> do
  state <- fetchIRCState
  let (nick,msg) = fromMsgEvent src msg'
      Just chanConfig = L.find (chanFilter chan) (channelConfigs state)
  liftIO $ putStrLn $ (T.unpack chan) ++ ": <" ++ nick ++ "> " ++ msg
  mapM_ (\f -> f chanConfig nick msg) [moderationHandler msg', greetingsHandler, giveawayHandler, cmdHandler]
  where
    chanFilter chan c = channel c == drop 1 (T.unpack chan)

cmdHandler :: ChannelConfig -> String -> String -> IRC IrcState ()
cmdHandler chan nick msg =
  let cmd = if length (words msg) == 0 then "" else map toLower $ head $ words msg
  in runCmd chan nick cmd (tail $ words msg) True

greetingsHandler :: ChannelConfig -> String -> String -> IRC IrcState ()
greetingsHandler chan nick msg = do
  state <- fetchIRCState
  greeting' <- liftIO $ query (db state) $ FetchGreeting (channel chan) nick
  case greeting' of
    Nothing -> pure ()
    Just greeting -> do
      now <- liftIO getCurrentTime
      let postGreeting = do
            liftIO $ update (db state) $ PostGreeting (channel chan) greeting now
            sendBS $ Privmsg (C.pack $ "#" ++ channel chan) (Right $ C.pack $ gmessage greeting)
      case gposted greeting of
        Nothing -> postGreeting
        Just lastPosted ->
          let diffTime = round $ toRational (diffUTCTime now lastPosted) :: Int64
          in if diffTime > (3600 * hours greeting) then postGreeting else pure ()

giveawayHandler :: ChannelConfig -> String -> String -> IRC IrcState ()
giveawayHandler chan nick msg = do
  state <- fetchIRCState
  gaways <- liftIO $ query (db state) $ FetchGiveaways $ channel chan
  mapM_ (\g -> if (map toLower $ channel chan) /= (map toLower nick) && L.isInfixOf (map toLower $ keyword g) (map toLower msg) then liftIO $ update (db state) $ AddToGiveaway (channel chan) (prize g) nick else pure ()) (F.toList gaways)

moderationHandler :: Either CTCPByteString a -> ChannelConfig -> String -> String -> IRC IrcState ()
moderationHandler raw chan nick msg = do
  state <- fetchIRCState
  mods  <- liftIO $ query (db state) $ FetchMods $ channel chan
  if elem nick mods then pure () else do
    filters <- sequence $ [blacklistFilter, linkFilter, lengthFilter, capFilter, symFilter, colorFilter, commercialFilter]
    case L.find (\(b,_) -> b) filters of
      Nothing -> pure ()
      Just (_,reason) -> do
        sendBS $ Privmsg (C.pack $ "#" ++ channel chan) (Right $ C.pack $ "/timeout " ++ nick ++ " 1 " ++ reason)
        display <- liftIO $ query (db state) $ FetchDisplayModeration (channel chan)
        if not display then pure () else sendBS $ Privmsg (C.pack $ "#" ++ channel chan) (Right $ C.pack $ "@" ++ nick ++ " timed out for: " ++ reason)
  where
    commercialFilter = do
      state <- fetchIRCState
      regex' <- liftIO $ query (db state) $ FetchSetting (channel chan) "commercials:regex" Nothing
      case regex' of
        Nothing -> pure (False,"")
        Just regexp -> do
          commercials' <- liftIO $ query (db state) $ FetchCommercials $ channel chan
          if length commercials' == 0 then pure (False,"") else do
            now <- liftIO $ getCurrentTime
            let lastC = fst $ last commercials'
                diffTime = toRational (diffUTCTime now lastC)
            if diffTime > 300 then pure (False,"")
            else do
              regexH <- liftIO $ regex [CaseInsensitive] (T.pack regexp)
              clonedR <- liftIO $ clone regexH
              liftIO $ setText clonedR (T.pack msg)
              found <- liftIO $ find clonedR (-1)
              pure $ if found then (True,"blacklisted phrase") else (False,"")
    colorFilter = do
      state  <- fetchIRCState
      colors <- liftIO $ query (db state) $ FetchColorFilter $ channel chan
      case raw of
        Left bs -> if colors && take 7 (C.unpack $ getUnderlyingByteString bs) == "\SOHACTION" then pure (True,"colors") else pure (False,"")
        _ -> pure (False,"")
    capFilter = do
      state <- fetchIRCState
      subs <- liftIO $ query (db state) $ FetchSubs $ channel chan
      if elem nick subs then pure (False,"")
      else do
        capsF <- liftIO $ query (db state) $ FetchCapFilter $ channel chan
        case capsF of
          Nothing -> pure (False,"")
          Just (per,trigger) ->
            let msg' = if length msg > 0 && head msg == '@' then L.intercalate " " (drop 1 (words msg)) else msg
            in if length msg' < trigger then pure (False,"")
               else let (caps,_) = L.partition (`elem` ['A'..'Z']) msg'
                    in pure $ if realToFrac (length caps) / realToFrac (length msg') >= realToFrac per / 100.0 then (True,"caps") else (False,"")
    symFilter = do
      state <- fetchIRCState
      symF  <- liftIO $ query (db state) $ FetchSymFilter $ channel chan
      case symF of
        Nothing -> pure (False,"")
        Just (per,trigger) ->
          let alpha = ['a' .. 'z'] ++ ['A' .. 'Z'] ++ ['0' .. '9'] ++ [' ']
              msg' = if length msg > 0 && head msg == '@' then L.intercalate " " (drop 1 (words msg)) else msg
          in if length msg' < trigger then pure (False,"")
             else let (_,syms) = L.partition (`elem` alpha) msg'
                  in pure $ if realToFrac (length syms) / realToFrac (length msg') >= realToFrac per / 100.0 then (True,"symbols") else (False,"")
    lengthFilter = do
      state <- fetchIRCState
      len' <- liftIO $ query (db state) $ FetchLength $ channel chan
      case len' of
        Nothing -> pure (False,"")
        Just len -> pure $ (length msg >= len,"message length")
    linkFilter = do
      state <- fetchIRCState
      links <- liftIO $ query (db state) $ FetchLinks $ channel chan
      permits <- liftIO $ query (db state) $ FetchPermits $ channel chan
      subs' <- liftIO $ query (db state) $ FetchSubs $ channel chan
      rgx <- liftIO $ query (db state) $ FetchSetting (channel chan) "links:subs" Nothing
      let subs = case rgx of
                   Nothing -> []
                   Just _ -> subs'
      if elem nick subs || length links == 0 || elem (map toLower nick) permits then pure (False,"")
      else do
        checks <- sequence (map (liftIO . detectURI links) (words msg))
        pure $ (any not checks,"links")
      where
        detectURI links w = do
          regex' <- regex [CaseInsensitive] urlRegex
          clonedR <- clone regex'
          setText clonedR (T.pack w)
          found <- find clonedR (-1)
          if found then do
            startI  <- start_ clonedR 0
            endI    <- end_ clonedR 0
            let word' = takeWord16 endI $ dropWord16 startI (T.pack w)
                word = T.unpack word'
                str' = if take 7 word == "http://" || take 8 word == "https://" then word else "http://" ++ word
                str  = if last str' == '/' then init str' else str'
            case U.parseURI str of
              Nothing -> pure True
              Just uri -> case U.uriAuthority uri of
                Nothing -> pure True
                Just auth -> pure $ any id $ map (matchLink uri auth) links
          else pure True
          where
            matchLink uri auth link =
              let regName = if take 4 (U.uriRegName auth) == "www." then drop 4 (U.uriRegName auth) else U.uriRegName auth
                  (host,path) = span (/='/') link
              in map toLower regName == map toLower host && map toLower (take (length path) (U.uriPath uri)) == map toLower path
    blacklistFilter = do
      state <- fetchIRCState
      blacklist <- liftIO $ query (db state) $ FetchBlacklist $ channel chan
      subs <- liftIO $ query (db state) $ FetchSubs $ channel chan
      filters <- liftIO $ sequence $ map (detectRegex subs) (F.toList blacklist)
      pure $ (any id filters, "blacklisted phrase")
      where
        detectRegex subs entry = do
          if exempt entry && elem nick subs then pure False else do
            regex' <- safeRegex [CaseInsensitive] (regexp entry)
            case regex' of
              Left e -> pure False
              Right regex -> do
                clonedR <- clone regex
                setText clonedR (T.pack msg)
                found <- find clonedR (-1)
                pure found

runDiscord :: AcidState BabbleState -> BotConfig -> [ChannelConfig] -> IO ()
runDiscord db botC chans = do
  case discord_token botC of
    Nothing -> forever $ threadDelay 3600000000
    Just token -> (WU.runSecureClient "gateway.discord.gg" 443 "/" (app token)) `E.catch` (discordEHandler token)
  where
    discordEHandler :: String -> E.SomeException -> IO ()
    discordEHandler token e = do
      threadDelay 10000000
      (WU.runSecureClient "gateway.discord.gg" 443 "/" (app token)) `E.catch` (discordEHandler token)
    app token conn = do
      forever $ do
        msg <- WS.receiveData conn
        let json = decode msg :: Maybe Value
        case json of
          Nothing -> pure ()
          Just (Object o) ->
            case HM.lookup "op" o of
              Nothing -> pure ()
              Just (Number n) ->
                case n of
                  0.0 -> do
                    let Just (String event) = HM.lookup "t" o
                    case event of
                      "MESSAGE_CREATE" ->
                        let Just (Object d) = HM.lookup "d" o
                            Just (String s) = HM.lookup "content" d
                            Just (String c) = HM.lookup "channel_id" d
                            content = T.unpack s
                            dchan = T.unpack c
                            cmd = if length (words content) == 0 then "" else map toLower $ head $ words content
                        in case HM.lookup "guild_id" d of
                             Nothing -> pure ()
                             Just (String guild) ->
                               case L.find chanFilter chans of
                                 Nothing -> pure ()
                                 Just chan -> runDiscordCmd db botC chan dchan cmd
                      _ -> pure ()
                  10.0 -> do
                    let Just (Object d) = HM.lookup "d" o
                    case HM.lookup "heartbeat_interval" d of
                      Nothing -> pure ()
                      Just (Number ms) -> do
                        repeatedTimer (WS.sendTextData conn $ encode $ object [ "op" Y..= (1::Int), "d" Y..= Null ]) (msDelay $ truncate ms)
                        WS.sendTextData conn $ encode $ object [ "op" Y..= (2::Int), "d" Y..= (object [ "token" Y..= token,  "properties" Y..= (object []) ]) ]
                  _ -> pure ()
      where
        chanFilter chan = case discord_id chan of
                            Nothing -> False
                            Just _ -> True

runWeb :: AcidState BabbleState -> BotConfig -> ChannelConfig -> IO ()
runWeb db botC chanC = do
  simpleHTTP (nullConf { Happstack.Server.port = web_port chanC }) routes
  where
    routes = msum $ api ++ [ dir "commands" $ webCommands db botC chanC
                           , dir "assets" $ serveDirectory DisableBrowsing ["index.html"] "web"
                           , basicAuth "babblebot" (M.singleton (web_user chanC) (web_pass chanC)) $ home db botC chanC
                           ]
    api = [ dir "api" $ dir "init" $ apiInit db chanC
          , dir "api" $ dir "update" $ apiUpdateStreamInfo db botC chanC
          , dir "api" $ dir "commands" $ dir "save" $ apiCommandSave db chanC
          , dir "api" $ dir "commands" $ dir "trash" $ apiCommandTrash db chanC
          , dir "api" $ dir "moderation" $ dir "blacklist" $ dir "save" $ apiBlacklistSave db chanC
          , dir "api" $ dir "moderation" $ dir "blacklist" $ dir "trash" $ apiBlacklistTrash db chanC
          ]

apiInit db chanC = do
  auth   <- liftIO $ query db $ FetchWebAuth (channel chanC)
  header <- fetchHeader "Authorization"
  if header /= C.pack auth then ok $ toResponse (""::String) else do
    Just chan <- liftIO $ query db $ FetchChannel (channel chanC)
    ok $ toResponse $ encode chan

apiCommandSave db chanC = do
  auth   <- liftIO $ query db $ FetchWebAuth (channel chanC)
  header <- fetchHeader "Authorization"
  if header /= C.pack auth then ok $ toResponse (""::String) else do
    Just chan <- liftIO $ query db $ FetchChannel (channel chanC)
    body <- fetchBody
    case (decode body :: Maybe APICommand) of
      Nothing -> badRequest $ toResponse (""::String)
      Just json -> do
        liftIO $ update db (AddCom (channel chanC) (Lib.name json) (content json) All)
        cmds <- liftIO $ query db $ FetchCommands (channel chanC)
        ok $ toResponse $ encode cmds

apiCommandTrash db chanC = do
  auth   <- liftIO $ query db $ FetchWebAuth (channel chanC)
  header <- fetchHeader "Authorization"
  if header /= C.pack auth then ok $ toResponse (""::String) else do
    Just chan <- liftIO $ query db $ FetchChannel (channel chanC)
    body <- fetchBody
    case (decode body :: Maybe APICommand) of
      Nothing -> badRequest $ toResponse (""::String)
      Just json -> do
        liftIO $ update db (DelCom (channel chanC) (Lib.name json))
        cmds <- liftIO $ query db $ FetchCommands (channel chanC)
        ok $ toResponse $ encode cmds

apiBlacklistSave db chanC = do
  auth   <- liftIO $ query db $ FetchWebAuth (channel chanC)
  header <- fetchHeader "Authorization"
  if header /= C.pack auth then ok $ toResponse (""::String) else do
    Just chan <- liftIO $ query db $ FetchChannel (channel chanC)
    body <- fetchBody
    case (decode body :: Maybe BlacklistEntry) of
      Nothing -> badRequest $ toResponse (""::String)
      Just json -> do
        liftIO $ update db (AddBlacklist (channel chanC) (regexp json) (exempt json))
        blacklist <- liftIO $ query db $ FetchBlacklist (channel chanC)
        ok $ toResponse $ encode blacklist

apiBlacklistTrash db chanC = do
  auth   <- liftIO $ query db $ FetchWebAuth (channel chanC)
  header <- fetchHeader "Authorization"
  if header /= C.pack auth then ok $ toResponse (""::String) else do
    Just chan <- liftIO $ query db $ FetchChannel (channel chanC)
    body <- fetchBody
    liftIO $ update db (DelBlacklist (channel chanC) (LBC.unpack body))
    blacklist <- liftIO $ query db $ FetchBlacklist (channel chanC)
    ok $ toResponse $ encode blacklist

apiUpdateStreamInfo db botC chanC = do
  auth   <- liftIO $ query db $ FetchWebAuth (channel chanC)
  header <- fetchHeader "Authorization"
  if header /= C.pack auth then ok $ toResponse (""::String) else do
    Just chan <- liftIO $ query db $ FetchChannel (channel chanC)
    body <- fetchBody
    case (decode body :: Maybe APIStreamInfo) of
      Nothing -> badRequest $ toResponse (""::String)
      Just json -> do
        let optsH = W.defaults & W.header "Client-ID" .~ [C.pack $ client_id botC]
            opts  = W.defaults & W.header "Accept" .~ [C.pack "application/vnd.twitchtv.v5+json"] & W.header "Authorization" .~ [C.pack $ "OAuth " ++ oauth chanC] & W.header "Client-ID" .~ [C.pack $ client_id botC]
        rsp' <- liftIO $ safePutWith opts ("https://api.twitch.tv/kraken/channels/" ++ chanID chan) $ object [ "channel" Y..= object [ "status" Y..= title json ] ]
        title <- case rsp' of
          Left err -> pure False
          Right rsp -> do
            let body = decode $ rsp ^. W.responseBody :: Maybe Value
            case body of
              Nothing -> pure False
              Just (Object o) ->
                case HM.lookup "status" o of
                  Nothing -> pure False
                  Just (String g) -> pure True
        rsp' <- liftIO $ safeGetWith optsH ("https://api.twitch.tv/helix/games?name=" ++ game json)
        game <- case rsp' of
                  Left err -> pure False
                  Right rsp -> do
                    let body = decode $ rsp ^. W.responseBody :: Maybe Value
                    case body of
                      Nothing -> pure False
                      Just (Object o) ->
                        case HM.lookup "data" o of
                          Nothing -> pure False
                          Just (Array a) ->
                            if length a == 0 then pure False -- ok $ toResponse (("Couldn't find a game matching: " ++ game json)::String)
                            else case V.head a of
                              Object o ->
                                case HM.lookup "name" o of
                                  Nothing -> pure False
                                  Just (String s) -> do
                                    rsp' <- liftIO $ safePutWith opts ("https://api.twitch.tv/kraken/channels/" ++ chanID chan) $ object [ "channel" Y..= object [ "game" Y..= game json ] ]
                                    case rsp' of
                                      Left err -> pure False
                                      Right rsp -> do
                                        let body = decode $ rsp ^. W.responseBody :: Maybe Value
                                        case body of
                                          Nothing -> pure False
                                          Just (Object o) ->
                                            case HM.lookup "game" o of
                                              Nothing -> pure False
                                              Just (String g) -> pure True
        ok $ toResponse $ encode $ object $ [ "title" Y..= title, "game" Y..= game ]

home :: AcidState BabbleState -> BotConfig -> ChannelConfig -> ServerPart Response
home db botC chanC = do
  token <- liftIO $ query db $ FetchWebAuth $ channel chanC
  ok $ toResponse $ do
    H.docType
    H.html $ do
      H.head $ do
        H.meta ! A.charset "utf-8"
        H.link ! A.rel "stylesheet" ! href "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css"
        H.link ! A.rel "stylesheet" ! href "https://fonts.googleapis.com/css?family=Open+Sans|Ubuntu"
        H.link ! A.rel "stylesheet" ! href "https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        H.title $ toHtml $ bot botC
        H.style "html { background-color: #F0EFEB; height: 100% }"
      H.body ! A.id "body" $ do
        H.script ! A.src "./assets/dist/app.js" $ ""
        H.script $ toHtml $ "Elm.Main.init({node:document.getElementById(\"body\"), flags: {name:'" ++ (bot botC) ++ "',channel:'" ++ (channel chanC) ++ "',client_id:'" ++ (client_id botC) ++ "',token:'" ++ token ++ "'}})"

webCommands :: AcidState BabbleState -> BotConfig -> ChannelConfig -> ServerPart Response
webCommands db botC chanC = do
  cmds <- liftIO $ query db $ FetchCommands $ channel chanC
  ok $ toResponse $ do
    H.docType
    H.html $ do
      H.head $ do
        H.meta ! A.charset "utf-8"
        H.link ! A.rel "stylesheet" ! href "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css"
        H.link ! A.rel "stylesheet" ! href "https://fonts.googleapis.com/css?family=Open+Sans|Ubuntu"
        H.title $ toHtml $ bot botC
        H.style "html { background-color: #F0EFEB; height: 100% }"
      H.body ! A.id "body" $ do
        H.script ! A.src "./assets/dist/commands.js" $ ""
        H.script $ toHtml $ "Elm.Main.init({node:document.getElementById(\"body\"), flags: {name:'" ++ (bot botC) ++ "',commands:" ++ cmdList cmds ++ "}})"
    where
      cmdList cmds = LBC.unpack $ encode $ object $ map (\c -> ((T.pack $ cmdName c), (String $ T.pack $ cmdContent c))) (F.toList cmds)

fetchBody :: ServerPart LB.ByteString
fetchBody = do
    req  <- askRq
    body <- liftIO $ takeRequestBody req
    case body of
        Just rqbody -> return . unBody $ rqbody
        Nothing     -> return ""

fetchHeader :: String -> ServerPart B.ByteString
fetchHeader str = do
    req <- askRq
    case getHeader str req of
        Just rqheader -> return rqheader
        Nothing       -> return ""

data APIStreamInfo = APIStreamInfo { title :: String, game :: String } deriving (Generic)
instance FromJSON APIStreamInfo

data APICommand = APICommand { name :: String, content :: String } deriving (Generic, Show)
instance FromJSON APICommand
