FROM toovs/babblebot:0.2.6.5
COPY . /babblebot
WORKDIR /babblebot
RUN stack build
CMD ["/tini", "--", "stack", "exec", "--", "babblebot"]
