# Babblebot

## Usage Guide

Haskell takes a large amount of memory during compiling, so I've set up docker to be able to easily run the bot on lower-end hardware. After cloning the repo and setting up your `config.yaml` and `docker-compose.yml` files, all that should be required to update and run the latest version of the bot should be: `git pull && docker-compose up --build`
