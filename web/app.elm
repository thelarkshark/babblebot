module Main exposing (..)

import String as S
import Char
import Browser
import Json.Decode exposing (Decoder, string, int, bool, map, map2, field, maybe, at, list, oneOf)
import Json.Encode as Encode
import Task
import Process
import Platform.Cmd as Cmd
import Css exposing (..)
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)
import Http exposing (..)

type alias Flags = { name : String, channel : String, client_id : String, token : String }

type Msg = SelectMenuItem String | InfoReceived (Result Http.Error Info) | DataReceived (Result Http.Error Data) | UpdateInfo Info | UpdateTitle String | UpdateGame String | UpdateCommand String String | InfoSuccess InfoR | InfoRevert | InfoRReceived (Result Http.Error InfoR) | CmdReceived (Result Http.Error (List Command)) | NewCmdReceived (Result Http.Error (List Command)) | CommandSave Command | CommandTrash Command | NewCommandSave | NewCmdSuccess | UpdateNewCommandName String | UpdateNewCommandContent String | NewBlacklistSave | BlacklistTrash BlacklistEntry | BlacklistReceived (Result Http.Error (List BlacklistEntry)) | NewBlacklistReceived (Result Http.Error (List BlacklistEntry)) | UpdateBlacklist BlacklistEntry String | UpdateNewEntryRegexp String | UpdateNewEntryExempt Bool | NewBlacklistSuccess

type alias Pane = { name : String, active : Bool, sections : List Section }
type alias Section = { name : String, error : Maybe String, data : SectionData }
type SectionData = Dashboard { title : Field, game : Field } | Commands { newCommand : Command, commands : List Command } | Blacklist { newEntry : BlacklistEntry, blacklist : List BlacklistEntry }
type alias Field = { content : Maybe String, status : Maybe Bool }
type alias Command = { name : String, content : String }

type alias Model = { name : String, token : String, panes : List Pane }

type alias Data = { commands : List Command, moderation : List Moderation }
type alias Moderation = { tag : String, contents : Contents }
type Contents = ContentBool Bool | ContentInt Int | ContentStrings (List String) | ContentInts (List Int) | ContentBlacklist (List BlacklistEntry)

type alias Info = { title : Maybe String, game : Maybe String }
type alias InfoR = { title : Bool, game : Bool }

type alias BlacklistEntry = { regexp : String, exempt : Bool }

initPanes : List Pane
initPanes = [
    Pane "dashboard" True [Section "info" Nothing (Dashboard { title = Field Nothing Nothing, game = Field Nothing Nothing })],
    Pane "commands" False [Section "commands" Nothing (Commands { commands = [], newCommand = Command "" "" })],
    Pane "moderation" False [Section "blacklist" Nothing (Blacklist { blacklist = [], newEntry = BlacklistEntry "" False })]
  ]

initModel : Model
initModel = { name = "", token = "", panes = initPanes }

dataDecoder : Decoder Data
dataDecoder = Json.Decode.map2 Data (field "commands" (Json.Decode.list cmdDecoder)) (field "moderation" (Json.Decode.list (Json.Decode.map2 Moderation (field "tag" string) (field "contents" (oneOf [Json.Decode.map ContentBool bool, Json.Decode.map ContentInt Json.Decode.int, Json.Decode.map ContentStrings (Json.Decode.list string), Json.Decode.map ContentInts (Json.Decode.list Json.Decode.int), Json.Decode.map ContentBlacklist blacklistDecoder])))))

cmdDecoder : Decoder Command
cmdDecoder = Json.Decode.map2 Command (field "cmdName" string) (field "cmdContent" string)

cmdRDecoder : Decoder (List Command)
cmdRDecoder = Json.Decode.list cmdDecoder

blacklistDecoder : Decoder (List BlacklistEntry)
blacklistDecoder = Json.Decode.list (Json.Decode.map2 BlacklistEntry (field "regexp" string) (field "exempt" bool))

blacklistEncoder entry =
  Encode.object
    [ ("regexp", Encode.string entry.regexp)
    , ("exempt", Encode.bool entry.exempt)
    ]

cmdEncoder cmd =
  Encode.object
    [ ("name", Encode.string cmd.name)
    , ("content", Encode.string cmd.content)
    ]

infoDecoder : Decoder Info
infoDecoder = Json.Decode.map2 Info (maybe (field "status" string)) (maybe (field "game" string))

infoRDecoder : Decoder InfoR
infoRDecoder = Json.Decode.map2 InfoR (field "title" bool) (field "game" bool)

infoEncoder info =
  Encode.object
    [ ("title", Encode.string (Maybe.withDefault "" info.title))
    , ("game", Encode.string (Maybe.withDefault "" info.game))
    ]

infoRequest channel client_id = infoDecoder
                                 |> Http.get ("https://api.twitch.tv/kraken/channels/" ++ channel ++ "?client_id=" ++ client_id)
                                 |> Http.send InfoReceived

dataRequest token = Http.send DataReceived (request token "GET" "/api/init" (Http.emptyBody) (Http.expectJson dataDecoder))

init : Flags -> ( Model, Cmd Msg )
init flags = ({ initModel | name = flags.name, token = flags.token }, Cmd.batch [(infoRequest flags.channel flags.client_id), (dataRequest flags.token)] )

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    SelectMenuItem name -> ({ model | panes = (List.map (\pane -> if pane.name == name then {pane|active=True} else {pane|active=False}) model.panes) }, Cmd.none)
    DataReceived (Ok data) -> ({ model | panes = updatedData model data }, Cmd.none)
    DataReceived (Err httpError) -> (model, Cmd.none)
    InfoReceived (Ok info) -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then updatedInfo pane info else pane) model.panes) }, Cmd.none)
    InfoReceived (Err httpError) -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then updatedInfoErr pane (createErrorMessage httpError) else pane) model.panes) }, Cmd.none)
    UpdateInfo data -> (model, Http.send InfoReceived (request model.token "POST" "/api/update" (Http.jsonBody (infoEncoder data)) (Http.expectJson infoDecoder)))
    UpdateTitle title -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then updatedTitle pane title else pane) model.panes) }, Cmd.none)
    UpdateGame game -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then updatedGame pane game else pane) model.panes) }, Cmd.none)
    InfoRReceived (Ok info) -> (model, Task.perform (\_ -> InfoSuccess info) (Task.succeed Nothing))
    InfoRReceived (Err httpError) -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then updatedInfoErr pane (createErrorMessage httpError) else pane) model.panes) }, Cmd.none)
    InfoSuccess info -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then updatedInfoSuccess pane info else pane) model.panes) }, Process.sleep (3 * 1000) |> Task.perform (\_ -> InfoRevert))
    InfoRevert -> ({ model | panes = (List.map (\pane -> if pane.name == "dashboard" then revertInfoSuccess pane else pane) model.panes) }, Cmd.none)
    NewCmdSuccess -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then clearNewCommand pane else pane) model.panes) }, Cmd.none)
    NewBlacklistSuccess -> ({ model | panes = (List.map (\pane -> if pane.name == "moderation" then clearNewBlacklist pane else pane) model.panes) }, Cmd.none)
    CommandSave cmd -> (model, Http.send CmdReceived (request model.token "POST" "/api/commands/save" (Http.jsonBody (cmdEncoder cmd)) (Http.expectJson cmdRDecoder)))
    CommandTrash cmd -> (model, Http.send CmdReceived (request model.token "POST" "/api/commands/trash" (Http.jsonBody (cmdEncoder cmd)) (Http.expectJson cmdRDecoder)))
    BlacklistTrash entry -> (model, Http.send BlacklistReceived (request model.token "POST" "/api/moderation/blacklist/trash" (Http.stringBody "application/json" entry.regexp) (Http.expectJson blacklistDecoder)))
    CmdReceived (Ok cmds) -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then updatedCommands pane cmds else pane) model.panes) }, Cmd.none)
    CmdReceived (Err httpError) -> (model, Cmd.none)
    NewCmdReceived (Ok cmds) -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then updatedCommands pane cmds else pane) model.panes) }, Task.perform (\_ -> NewCmdSuccess) (Task.succeed Nothing))
    NewCmdReceived (Err httpError) -> (model, Cmd.none)
    BlacklistReceived (Ok blacklist) -> ({ model | panes = (List.map (\pane -> if pane.name == "moderation" then updatedBlacklist pane blacklist else pane) model.panes) }, Cmd.none)
    BlacklistReceived (Err httpError) -> (model, Cmd.none)
    NewBlacklistReceived (Ok blacklist) -> ({ model | panes = (List.map (\pane -> if pane.name == "moderation" then updatedBlacklist pane blacklist else pane) model.panes) }, Task.perform (\_ -> NewBlacklistSuccess) (Task.succeed Nothing))
    NewBlacklistReceived (Err httpError) -> (model, Cmd.none)
    UpdateCommand name content -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then updateCommand pane name content else pane) model.panes) }, Cmd.none)
    UpdateNewCommandName name -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then updateNewCommandName pane name else pane) model.panes) }, Cmd.none)
    UpdateNewCommandContent content -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then updateNewCommandContent pane content else pane) model.panes) }, Cmd.none)
    UpdateNewEntryRegexp content -> ({ model | panes = (List.map (\pane -> if pane.name == "moderation" then updateNewEntryRegexp pane content else pane) model.panes) }, Cmd.none)
    UpdateNewEntryExempt bool -> ({ model | panes = (List.map (\pane -> if pane.name == "moderation" then updateNewEntryExempt pane bool else pane) model.panes) }, Cmd.none)
    UpdateBlacklist entry content -> ({ model | panes = (List.map (\pane -> if pane.name == "commands" then updateBlacklist pane entry content else pane) model.panes) }, Cmd.none)
    NewCommandSave ->
      case fetchPane "commands" model.panes of
        Nothing -> (model, Cmd.none)
        Just pane ->
          case fetchSection "commands" pane.sections of
            Nothing -> (model, Cmd.none)
            Just section ->
              case section.data of
                Commands data -> (model, Http.send NewCmdReceived (request model.token "POST" "/api/commands/save" (Http.jsonBody (cmdEncoder data.newCommand)) (Http.expectJson cmdRDecoder)))
                _ -> (model, Cmd.none)
    NewBlacklistSave ->
      case fetchPane "moderation" model.panes of
        Nothing -> (model, Cmd.none)
        Just pane ->
          case fetchSection "blacklist" pane.sections of
            Nothing -> (model, Cmd.none)
            Just section ->
              case section.data of
                Blacklist data -> (model, Http.send NewBlacklistReceived (request model.token "POST" "/api/moderation/blacklist/save" (Http.jsonBody (blacklistEncoder data.newEntry)) (Http.expectJson blacklistDecoder)))
                _ -> (model, Cmd.none)

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

main : Program Flags Model Msg
main =
  Browser.element
    { init = init
    , view = view >> toUnstyled
    , update = update
    , subscriptions = subscriptions
    }

view : Model -> Html Msg
view model =
    node "body" [ cssApp ]
        [ div [ id "header", cssHeader ] (htmlHeader model)
        , div [ id "container", cssContainer ]
            [ div [ id "nav", cssNav ] (htmlNav model)
            , div [ id "content", cssContent ] (htmlContent model)
            ]
        ]

--- Helper Functions ---

createErrorMessage : Http.Error -> String
createErrorMessage httpError =
  case httpError of
    Http.BadUrl message -> message
    Http.Timeout -> "Server is taking too long to respond. Please try again later."
    Http.NetworkError -> "It appears you don't have an Internet connection right now."
    Http.BadStatus response -> response.status.message
    Http.BadPayload message response -> message

request : String -> String -> String -> Body -> Expect a -> Http.Request a
request token method url body decoder =
    Http.request
        { method = method
        , headers = [Http.header "Authorization" token]
        , url = url
        , body = body
        , expect = decoder
        , timeout = Nothing
        , withCredentials = False
        }

fetchPane : String -> List Pane -> Maybe Pane
fetchPane name panes = List.head (List.filter (\p -> p.name == name) panes)

fetchSection : String -> List Section -> Maybe Section
fetchSection name sections = List.head (List.filter (\s -> s.name == name) sections)

fetchTag : String -> List Moderation -> Maybe Moderation
fetchTag name mods = List.head (List.filter (\m -> m.tag == name) mods)

capitalize : String -> String
capitalize str =
  case S.uncons str of
   Nothing -> ""
   Just (head, tail) -> S.cons (Char.toUpper head) tail

--- HTML ---

htmlHeader model =
  [ div [ id "title", cssHeaderTitle ] [ text model.name ]
  ]

htmlNav model =
  [ div [ id "menu", cssMenu ] (List.map genMenuItem model.panes)
  ]

htmlContent model =
  [ div [ id "panes" ] (htmlPanes model)
  ]

htmlPanes model = List.map genPane model.panes

genMenuItem pane = div [ cssMenuItem pane.active, onClick (SelectMenuItem pane.name) ] [ text (capitalize pane.name) ]

genPane pane = div [ id pane.name, cssPane pane.active ] (List.concatMap genSection pane.sections)

genSection section =
  case section.name of
    "info" -> htmlSectionInfo section
    "commands" -> htmlSectionCommands section
    "blacklist" -> htmlSectionBlacklist section
    _ -> []

htmlSectionInfo section =
  case section.data of
    Dashboard data ->
      let titleClass = case data.title.status of
                         Nothing -> "input"
                         Just True -> "input is-success"
                         Just False -> "input is-danger"
          gameClass = case data.game.status of
                             Nothing -> "input"
                             Just True -> "input is-success"
                             Just False -> "input is-danger"
      in [ div [ class "title is-4" ] [ text "Stream Info" ]
         , div [ cssFields ]
             [ div [ class "field" ]
                 [ label [ class "label" ] [ text "Title" ]
                 , input [ class titleClass, cssInput, value (Maybe.withDefault "" data.title.content), onInput UpdateTitle ] []
                 ]
             , div [ class "field" ]
                 [ label [ class "label" ] [ text "Game" ]
                 , input [ class gameClass, cssInput, value (Maybe.withDefault "" data.game.content), onInput UpdateGame ] []
                 ]
             , div [] [ text (Maybe.withDefault "" section.error) ]
             ]
         , div [ cssButtons ] [ a [ class "button is-link", onClick (UpdateInfo (Info data.title.content data.game.content)) ] [ text "Save" ] ]
         ]
    _ -> []

htmlSectionCommands section =
  case section.data of
    Commands data ->
      [ div [ class "title is-4" ] [ text "Commands" ]
      , node "table" [ class "table is-striped", cssTable ]
          [ tbody [] (htmlNewCommand data :: (List.map genCommand (List.sortBy .name data.commands))) ]
      ]
    _ -> []

htmlNewCommand data =
  tr []
    [ td [] [ input [ class "input", cssNewCmd, placeholder "!newcommand", onInput UpdateNewCommandName, value data.newCommand.name ] [] ]
    , td [] [ input [ class "input", cssCmd, placeholder "command content", onInput UpdateNewCommandContent, value data.newCommand.content ] [] ]
    , td [ cssTd ] [ span [ class "icon", cssTableIcon, cssSave, onClick NewCommandSave ] [ i [ class "fas fa-save" ] [] ] ]
    ]

genCommand cmd =
  tr []
    [ td [ cssTd ] [ text cmd.name ]
    , td [] [ input [ class "input", cssCmd, value cmd.content, onInput (UpdateCommand cmd.name) ] [] ]
    , td [ cssTd ]
        [ span [ class "icon", cssTableIcon, cssSave, onClick (CommandSave cmd) ] [ i [ class "fas fa-save" ] [] ]
        , span [ class "icon", cssTableIcon, cssTrash, onClick (CommandTrash cmd) ] [ i [ class "fas fa-trash" ] [] ]
        ]
    ]

htmlSectionBlacklist section =
  case section.data of
    Blacklist data ->
      [ div [ class "title is-4" ] [ text "Blacklist" ]
      , node "table" [ class "table is-striped", cssTable ]
          [ tbody [] (htmlNewBlacklist data :: (List.map genBlacklist (List.sortBy .regexp data.blacklist))) ]
      ]
    _ -> []

htmlNewBlacklist data =
  tr []
    [ td [] [ input [ class "input", cssNewEntry, placeholder "new regex", onInput UpdateNewEntryRegexp, value data.newEntry.regexp ] [] ]
    , td [] [ input [ type_ "checkbox", cssCheckbox, onCheck UpdateNewEntryExempt, Html.Styled.Attributes.checked data.newEntry.exempt ] [] ]
    , td [ cssTd ] [ span [ class "icon", cssTableIcon, cssSave, onClick NewBlacklistSave ] [ i [ class "fas fa-save" ] [] ] ]
    ]

genBlacklist entry =
  tr []
    [ td [] [ input [ class "input", cssCmd, value (entry.regexp), readonly True, onInput (UpdateBlacklist entry) ] [] ]
    , td [] [ input [ type_ "checkbox", cssCheckbox, Html.Styled.Attributes.disabled True, Html.Styled.Attributes.checked entry.exempt ] [] ]
    , td [ cssTd ]
        [ span [ class "icon", cssTableIcon, cssTrash, onClick (BlacklistTrash entry) ] [ i [ class "fas fa-trash" ] [] ] ]
    ]

--- CSS ---

orange = hex "#C43F1D"
black  = hex "#1D1C1F"
white  = hex "#F0EFEB"
green  = hex "#8F771F"
blue   = hex "#45806F"

success = hex "#23d160"
danger  = hex "#ff3860"

cssApp = css
  [ fontFamilies ["Open Sans"]
  , Css.height (pct 100)
  ]

cssContainer = css
  [ minHeight (calc (pct 100) minus (px 40))
  , position relative
  ]

cssContent = css
  [ marginLeft (px 260)
  ]

cssHeader = css
  [ backgroundColor black
  , Css.height (px 40)
  ]

cssHeaderTitle = css
  [ Css.width (px 260)
  , Css.height (px 40)
  , color white
  , backgroundColor blue
  , fontSize (Css.em 1.4)
  , fontFamilies ["Ubuntu"]
  , paddingTop (px 3)
  , paddingLeft (px 10)
  ]

cssNav = css
  [ position absolute
  , top (px 0)
  , left (px 0)
  , bottom (px 0)
  , Css.width (px 260)
  , backgroundColor black
  ]

cssMenu = css
  [ marginTop (px 20)
  , marginLeft (px 20)
  ]

cssMenuItem active = css
  [ color white
  , fontFamilies ["Ubuntu"]
  , fontSize (Css.em 1.4)
  , marginBottom (px 10)
  , padding2 (px 4) (px 10)
  , backgroundColor (if active then blue else black)
  , hover
      [ cursor pointer
      , backgroundColor blue
      ]
  ]

cssPane active = css
  ([ padding2 (px 27) (px 60)
   ] ++ if active then [] else [ display none ])

cssFields = css
  [
  ]

cssInput = css
  [ Css.width (px 400)
  ]

cssButtons = css
  [ marginTop (px 30)
  ]

cssSave = css
  [ hover
      [ color success
      ]
  ]

cssTrash = css
  [ hover
      [ color danger
      ]
  ]

cssTable = css
  [ fontFamilies ["Ubuntu"]
  ]

cssTd = css
  [ important (verticalAlign middle)
  ]

cssCheckbox = css
  [ marginTop (px 11)
  ]

cssTableIcon = css
  [ hover
      [ cursor pointer
      ]
  ]

cssNewCmd = css
  [ Css.width (px 150)
  , fontFamilies ["Open Sans"]
  ]

cssNewEntry = css
  [ fontFamilies ["Open Sans"]
  ]

cssCmd = css
  [ Css.width (px 600)
  , fontFamilies ["Open Sans"]
  ]

--- Model Updates ---

updatedData : Model -> Data -> List Pane
updatedData model data = List.map (updateData data) model.panes

updateData : Data -> Pane -> Pane
updateData data pane =
  case pane.name of
    "dashboard" -> pane
    "commands" ->
      case fetchSection "commands" pane.sections of
        Nothing -> pane
        Just section ->
          case section.data of
            Commands cdata ->
              let newData = {cdata|commands=data.commands}
                  newSection = {section|data=Commands newData}
                  newSections = List.map (\s -> if s.name == "commands" then newSection else s) pane.sections
              in {pane|sections=newSections}
            _ -> pane
    "moderation" ->
      case fetchSection "blacklist" pane.sections of
        Nothing -> pane
        Just section ->
          case section.data of
            Blacklist bdata ->
              case fetchTag "Blacklist" data.moderation of
                Nothing -> pane
                Just mod ->
                  case mod.contents of
                    ContentBlacklist entries ->
                      let newData = {bdata|blacklist=entries}
                          newSection = {section|data=Blacklist newData}
                          newSections = List.map (\s -> if s.name == "blacklist" then newSection else s) pane.sections
                      in {pane|sections=newSections}
                    _ -> pane
            _ -> pane
    _ -> pane


updatedCommands : Pane -> List Command -> Pane
updatedCommands pane cmds = case fetchSection "commands" pane.sections of
                              Nothing -> pane
                              Just section ->
                                case section.data of
                                  Commands data ->
                                    let newData = {data|commands = cmds}
                                        newSection = {section|data=Commands newData}
                                        newSections = List.map (\s -> if s.name == "commands" then newSection else s) pane.sections
                                    in {pane|sections=newSections}
                                  _ -> pane

updateCommand pane name content = case fetchSection "commands" pane.sections of
                                    Nothing -> pane
                                    Just section ->
                                      case section.data of
                                        Commands data ->
                                          let newData = {data|commands = List.map (\c -> if c.name == name then {c|content=content} else c) data.commands }
                                              newSection = {section|data=Commands newData}
                                              newSections = List.map (\s -> if s.name == "commands" then newSection else s) pane.sections
                                          in {pane|sections=newSections}
                                        _ -> pane

updateNewCommandName pane name = case fetchSection "commands" pane.sections of
                                   Nothing -> pane
                                   Just section ->
                                     case section.data of
                                       Commands data ->
                                         let cmd = data.newCommand
                                             newCmd = {cmd|name=name}
                                             newData = {data|newCommand=newCmd}
                                             newSection = {section|data=Commands newData}
                                             newSections = List.map (\s -> if s.name == "commands" then newSection else s) pane.sections
                                         in {pane|sections=newSections}
                                       _ -> pane

updateNewCommandContent pane content = case fetchSection "commands" pane.sections of
                                         Nothing -> pane
                                         Just section ->
                                           case section.data of
                                             Commands data ->
                                               let cmd = data.newCommand
                                                   newCmd = {cmd|content=content}
                                                   newData = {data|newCommand=newCmd}
                                                   newSection = {section|data=Commands newData}
                                                   newSections = List.map (\s -> if s.name == "commands" then newSection else s) pane.sections
                                               in {pane|sections=newSections}
                                             _ -> pane

clearNewCommand pane = case fetchSection "commands" pane.sections of
                         Nothing -> pane
                         Just section ->
                           case section.data of
                             Commands data ->
                               let newData = {data|newCommand = Command "" ""}
                                   newSection = {section|data=Commands newData}
                                   newSections = List.map (\s -> if s.name == "commands" then newSection else s) pane.sections
                               in {pane|sections=newSections}
                             _ -> pane

clearNewBlacklist pane = case fetchSection "blacklist" pane.sections of
                           Nothing -> pane
                           Just section ->
                             case section.data of
                               Blacklist data ->
                                 let newData = {data|newEntry=BlacklistEntry "" False}
                                     newSection = {section|data=Blacklist newData}
                                     newSections = List.map (\s -> if s.name == "blacklist" then newSection else s) pane.sections
                                 in {pane|sections=newSections}
                               _ -> pane

updatedBlacklist : Pane -> List BlacklistEntry -> Pane
updatedBlacklist pane blacklist = case fetchSection "blacklist" pane.sections of
                                    Nothing -> pane
                                    Just section ->
                                      case section.data of
                                        Blacklist data ->
                                          let newData = {data|blacklist=blacklist}
                                              newSection = {section|data=Blacklist newData}
                                              newSections = List.map (\s -> if s.name == "blacklist" then newSection else s) pane.sections
                                          in {pane|sections=newSections}
                                        _ -> pane

updateBlacklist pane entry content = case fetchSection "blacklist" pane.sections of
                                       Nothing -> pane
                                       Just section ->
                                         case section.data of
                                           Blacklist data ->
                                             let newData = {data|blacklist = List.map (\e -> if e.regexp == entry.regexp then {e|regexp=content} else e) data.blacklist }
                                                 newSection = {section|data=Blacklist newData}
                                                 newSections = List.map (\s -> if s.name == "blacklist" then newSection else s) pane.sections
                                             in {pane|sections=newSections}
                                           _ -> pane

updateNewEntryRegexp pane content = case fetchSection "blacklist" pane.sections of
                                      Nothing -> pane
                                      Just section ->
                                        case section.data of
                                          Blacklist data ->
                                            let entry = data.newEntry
                                                newEntry = {entry|regexp=content}
                                                newData = {data|newEntry=newEntry}
                                                newSection = {section|data=Blacklist newData}
                                                newSections = List.map (\s -> if s.name == "blacklist" then newSection else s) pane.sections
                                            in {pane|sections=newSections}
                                          _ -> pane

updateNewEntryExempt pane exempt = case fetchSection "blacklist" pane.sections of
                                     Nothing -> pane
                                     Just section ->
                                       case section.data of
                                         Blacklist data ->
                                           let entry = data.newEntry
                                               newEntry = {entry|exempt=exempt}
                                               newData = {data|newEntry=newEntry}
                                               newSection = {section|data=Blacklist newData}
                                               newSections = List.map (\s -> if s.name == "blacklist" then newSection else s) pane.sections
                                           in {pane|sections=newSections}
                                         _ -> pane

updatedInfo pane infoR = case fetchSection "info" pane.sections of
                           Nothing -> pane
                           Just info ->
                             case info.data of
                               Dashboard data ->
                                 let newTitle = Field infoR.title data.title.status
                                     newGame = Field infoR.game data.game.status
                                     newData = {data|title=newTitle,game=newGame}
                                     newInfo = {info|data=Dashboard newData}
                                     newSections = List.map (\s -> if s.name == "info" then newInfo else s) pane.sections
                                 in {pane|sections=newSections}
                               _ -> pane

updatedInfoErr pane err = case fetchSection "info" pane.sections of
                            Nothing -> pane
                            Just info ->
                              let newInfo = {info|error=Just err}
                                  newSections = List.map (\s -> if s.name == "info" then newInfo else s) pane.sections
                              in {pane|sections=newSections}

updatedInfoSuccess pane infoR = case fetchSection "info" pane.sections of
                                  Nothing -> pane
                                  Just info ->
                                    case info.data of
                                      Dashboard data ->
                                        let newTitle = Field data.title.content (Just infoR.title)
                                            newGame = Field data.game.content (Just infoR.game)
                                            newData = {data|title=newTitle,game=newGame}
                                            newInfo = {info|data=Dashboard newData}
                                            newSections = List.map (\s -> if s.name == "info" then newInfo else s) pane.sections
                                        in {pane|sections=newSections}
                                      _ -> pane

revertInfoSuccess pane = case fetchSection "info" pane.sections of
                           Nothing -> pane
                           Just info ->
                             case info.data of
                               Dashboard data ->
                                 let newTitle = Field data.title.content Nothing
                                     newGame = Field data.game.content Nothing
                                     newData = {data|title=newTitle,game=newGame}
                                     newInfo = {info|data=Dashboard newData}
                                     newSections = List.map (\s -> if s.name == "info" then newInfo else s) pane.sections
                                 in {pane|sections=newSections}
                               _ -> pane

updatedTitle pane title = case fetchSection "info" pane.sections of
                            Nothing -> pane
                            Just info ->
                              case info.data of
                                Dashboard data ->
                                  let newField = Field (Just title) data.title.status
                                      newData = {data|title=newField}
                                      newInfo = {info|data=Dashboard newData}
                                      newSections = List.map (\s -> if s.name == "info" then newInfo else s) pane.sections
                                  in {pane|sections=newSections}
                                _ -> pane

updatedGame pane game = case fetchSection "info" pane.sections of
                          Nothing -> pane
                          Just info ->
                            case info.data of
                              Dashboard data ->
                                let newField = Field (Just game) data.game.status
                                    newData = {data|game=newField}
                                    newInfo = {info|data=Dashboard newData}
                                    newSections = List.map (\s -> if s.name == "info" then newInfo else s) pane.sections
                                in {pane|sections=newSections}
                              _ -> pane
