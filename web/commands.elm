module Main exposing (..)

import Json.Decode exposing (Decoder, Value, string, keyValuePairs, decodeValue, map)
import Css exposing (..)
import Browser
import Html
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (..)
import Html.Styled.Events exposing (..)

decoder : Decoder (List (String, String))
decoder = keyValuePairs string

type alias Flags = { name : String, commands : Json.Decode.Value }

type Msg = NoOp

type alias Model = { name : String, commands : List (String, String) }

initModel : Model
initModel = { name = "", commands = [] }

init : Flags -> ( Model, Cmd Msg )
init flags = case decodeValue decoder flags.commands of
               Ok cmds -> ({ initModel | name = flags.name, commands = List.sortBy Tuple.first cmds }, Cmd.none )
               Err err -> ({ initModel | name = flags.name, commands = [] }, Cmd.none )

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    NoOp -> ( model, Cmd.none )

subscriptions : Model -> Sub Msg
subscriptions model = Sub.none

main : Program Flags Model Msg
main =
  Browser.element
    { init = init
    , view = view >> toUnstyled
    , update = update
    , subscriptions = subscriptions
    }

orange = hex "#C43F1D"
black  = hex "#1D1C1F"
white  = hex "#F0EFEB"
green  = hex "#8F771F"
blue   = hex "#45806F"

view : Model -> Html Msg
view model =
    node "body" [ cssApp ]
        [ div [ id "header", cssHeader ] (htmlHeader model)
        , div [ id "nav", cssNav ] (htmlNav model)
        , div [ id "content", cssContent ] (htmlContent model)
        ]

cssApp = css
  [ fontFamilies ["Open Sans"]
  , Css.height (pct 100)
  ]

htmlHeader model =
  [ div [ id "title", cssHeaderTitle ] [ text model.name ]
  ]

cssHeader = css
  [ backgroundColor black
  , Css.height (px 40)
  ]

cssHeaderTitle = css
  [ Css.width (px 260)
  , Css.height (px 40)
  , color white
  , backgroundColor blue
  , fontSize (Css.em 1.4)
  , fontFamilies ["Ubuntu"]
  , paddingTop (px 3)
  , paddingLeft (px 10)
  ]

htmlNav model =
  [ div [ id "menu", cssMenu ] (List.map (\(k,v) -> genMenuItem k) model.commands)
  ]

htmlContent model =
  [ div [ id "menu", cssMenu ] (List.map (\(k,v) -> genMenuContent v) model.commands)
  ]

cssContent = css
  [ marginLeft (px 260)
  , overflowX scroll
  ]

cssNav = css
  [ float left
  , minHeight (calc (pct 100) minus (px 40))
  , Css.width (px 260)
  , backgroundColor black
  ]

genMenuItem : String -> Html Msg
genMenuItem name = div [ class "menu-item", cssMenuItem ] [ text name ]

genMenuContent : String -> Html Msg
genMenuContent name = div [ class "menu-content", cssMenuContent ] [ text name ]

cssMenu = css
  [ marginTop (px 20)
  , marginLeft (px 20)
  ]

cssMenuItem = css
  [ color white
  , fontFamilies ["Ubuntu"]
  , padding2 (px 2) (px 6)
  , backgroundColor black
  , hover
      [ cursor pointer
      , backgroundColor blue
      ]
  ]

cssMenuContent = css
  [ padding2 (px 2) (px 6)
  , whiteSpace noWrap
  ]
