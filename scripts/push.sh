#!/bin/bash

docker build -t babblebot -f Dockerfile.build .
docker tag babblebot toovs/babblebot:$(echo $1)
docker push toovs/babblebot:$(echo $1)
